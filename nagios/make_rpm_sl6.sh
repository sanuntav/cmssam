#!/bin/sh

if [ -z "$NAG_TMP" ] ; then
	echo "NAG_TMP undefined"
	exit 1
fi
cd $NAG_TMP
if [ -z "$NAG_SCRIPTS" ] ; then
	echo "NAG_SCRIPTS undefined"
	exit 1
fi
if [ -z "$NAG_DEST" ] ; then
	echo "NAG_DEST undefined"
	exit 1
fi
ver=$1
if [ -z "$ver" ] ; then
	echo "Specify version!"
	exit 1
fi
specver=`grep 'Version:' $NAG_SCRIPTS/grid-monitoring-probes-org.cms-etf.spec | cut -d' ' -f2`
if [ "$specver" != $ver ] ; then
	echo "Spec file not updated!"
	exit 1
fi
rm -rf tmp/nagios-plugins-wlcg-org.cms-$ver
mkdir -p tmp/nagios-plugins-wlcg-org.cms-$ver
cp -a org.cms tmp/nagios-plugins-wlcg-org.cms-$ver
cp -a config tmp/nagios-plugins-wlcg-org.cms-$ver
cd tmp
tar zcf nagios-plugins-wlcg-org.cms-$ver.tgz nagios-plugins-wlcg-org.cms-$ver
mkdir -p $NAG_TMP/SOURCES
mkdir -p $NAG_TMP/BUILD
mkdir -p $NAG_TMP/RPMS
mv nagios-plugins-wlcg-org.cms-$ver.tgz $NAG_TMP/SOURCES
cd ..
cat > .rpmmacros <<EOF
%_topdir        $NAG_TMP
%_dbpath        $NAG_TMP/db
%_tmppath       $NAG_TMP/rpm-tmp
%_sourcedir     $NAG_TMP/SOURCES
EOF
mkdir $NAG_TMP/SRPMS
export HOME=`pwd`
rpmbuild -ba --define "_source_filedigest_algorithm md5"  --define "_binary_filedigest_algorithm md5" $NAG_SCRIPTS/grid-monitoring-probes-org.cms-etf.spec
if [ $? -eq 0 ] ; then
        cp -i $NAG_TMP/RPMS/noarch/nagios-plugins-wlcg-org.cms-*.noarch.rpm $NAG_DEST/
        cp -i $NAG_TMP/SRPMS/nagios-plugins-wlcg-org.cms-*.src.rpm $NAG_DEST/
fi
rm -rf BUILD RPMS SRPMS rpm-tmp SOURCES tmp
