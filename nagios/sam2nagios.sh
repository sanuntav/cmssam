#!/bin/bash

if [ -z "$NAG_TMP" ] ; then
	echo "NAG_TMP undefined"
	exit 1
fi
BASEDIR=$NAG_TMP
SAMDIR=$BASEDIR/cmssam
NAGDIR=$BASEDIR
NAGCONF=$SAMDIR/nagios/config
rm -rf $NAGDIR/org.cms/*
mkdir -p $NAGDIR/org.cms/wnjob/org.cms.lcgadmin/etc/wn.d/org.cms
mkdir -p $NAGDIR/org.cms/wnjob/org.cms.production/etc/wn.d/org.cms
cp -aL $NAGCONF/org.cms.lcgadmin $NAGDIR/org.cms/wnjob/
cp -aL $NAGCONF/org.cms.production $NAGDIR/org.cms/wnjob/
cp $SAMDIR/SiteTests/SE/srmvometrics.py $SAMDIR/SiteTests/SE/cmssam_xrootd_endpnt.py $NAGDIR/org.cms/
chmod +x $NAGDIR/org.cms/srmvometrics.py $NAGDIR/org.cms/cmssam_xrootd_endpnt.py
mkdir -p $NAGDIR/config
cp -L $NAGCONF/emi.ce.CREAMCE.conf -L $NAGCONF/cms_glexec $NAGCONF/etf_plugin_cms.py $NAGCONF/cms_glexec-etf $NAGDIR/config/
srcdir=$SAMDIR
dstdir=$NAGDIR/org.cms/wnjob/org.cms/probes/org.cms

sensor='testjob'
modules='FroNtier MonteCarlo testjob'
for i in $modules ; do
   cd $srcdir/SiteTests/$i
   for j in `find . -not -regex '.*\(CVS\).*'` ; do
      if [ -d $j ] ; then
         mkdir -p $dstdir/$sensor/$j
      else
         cp $j $dstdir/$sensor/`dirname $j`
      fi
   done
done

cd $srcdir/nagios/
dstdir=$NAGDIR/org.cms/wnjob/
for j in `find org.cms.glexec -not -regex '.*\(CVS\).*'` ; do
   if [ -d $j ] ; then
      mkdir -p $dstdir/$j
   else
      cp $j $dstdir/`dirname $j`
   fi
done
cd $BASEDIR
