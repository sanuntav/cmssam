#!/bin/bash

source /usr/bin/etf-init.sh

cat << "EOF"
 _____ _____ _____    ____ __  __ ____
| ____|_   _|  ___|  / ___|  \/  / ___|
|  _|   | | | |_    | |   | |\/| \___ \
| |___  | | |  _|   | |___| |  | |___) |
|_____| |_| |_|      \____|_|  |_|____/
========================================
EOF

print_header

etf_update

start_xinetd

etf_init

copy_certs

apache_init

config_web_access

config_alerts

init_api

config_htcondor

config_etf

fix_cmk_theme

etf_start

fix_live_ip6

generate_test_cert

start_oidc_agent

echo "Fetching CMS credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --vo-fqan /cms/Role=lcgadmin --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo cms --lifetime 24 --name NagiosRetrieve-ETF-cms -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms-Role_lcgadmin --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --vo-fqan /cms/Role=production --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo cms --lifetime 24 --name NagiosRetrieve-ETF-cms -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms-Role_production --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo cms --lifetime 24 --name NagiosRetrieve-ETF-cms -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

echo "Initialising tokens ..."
su - etf -c "oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/etf_cms_ce.key etf_cms_ce"
su - etf -c "/usr/lib64/nagios/plugins/refresh_token -t 890 --token-config etf-cms-ce --token-time 21600 -x /opt/omd/sites/etf/etc/nagios/globus/cms-ce.token"

echo "Copying credentials ..."
/bin/cp -f /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec/probes/org.cms.glexec/testjob/tests/payloadproxy-t2
/bin/chown ${CHECK_MK_SITE} /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec/probes/org.cms.glexec/testjob/tests/payloadproxy-t2
/bin/chmod go+r /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec/probes/org.cms.glexec/testjob/tests/payloadproxy-t2
/bin/cp -f /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms-Role_production /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests/prodproxy
/bin/chown ${CHECK_MK_SITE} /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests/prodproxy
/bin/chmod go+r /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests/prodproxy

etf_wait