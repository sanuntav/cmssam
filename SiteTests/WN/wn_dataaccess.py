#!/usr/bin/python3
# ########################################################################### #
#
# SAM WorkerNode data-access probe of CMS
#
# ########################################################################### #



import os, sys
import platform
import time, calendar
import socket
import re
import pwd
import subprocess
import OpenSSL
import base64
import json
import xml.etree.ElementTree
import random
import shutil
import traceback
import argparse
# ########################################################################### #



# global variables:
SWNDA_VERSION = "v0.00.01"
#
SWNDA_FILES = [ "/store/mc/SAM/GenericTTbar/AODSIM/" + \
                    "CMSSW_9_2_6_91X_mcRun1_realistic_v2-v1/00000/" + \
                    "A64CCCF2-5C76-E711-B359-0CC47A78A3F8.root",
                "/store/mc/SAM/GenericTTbar/AODSIM/" + \
                    "CMSSW_9_2_6_91X_mcRun1_realistic_v2-v1/00000/" + \
                    "AE237916-5D76-E711-A48C-FA163EEEBFED.root",
                "/store/mc/SAM/GenericTTbar/AODSIM/" + \
                    "CMSSW_9_2_6_91X_mcRun1_realistic_v2-v1/00000/" + \
                    "CE860B10-5D76-E711-BCA8-FA163EAA761A.root" ]
#
SWNDA_CMSSW_LIST = {
   ('x86_64', 'rhel6'): [("CMSSW_10_5_0", "slc6_amd64_gcc700", "xml"),],
   ('x86_64', 'rhel7'): [("CMSSW_12_4_9", "slc7_amd64_gcc10",  "xml"), 
                         ("CMSSW_12_6_4", "slc7_amd64_gcc10",  "json")],
   ('x86_64', 'rhel8'): [("CMSSW_13_1_0", "el8_amd64_gcc10",   "json")] }
#
SWNDA_VERBOSITY = 0
#
SWNDA_OSID = (None, None)
#
SWNDA_SC_SITE = None
SWNDA_SC_SUBSITE = None
SWNDA_SC_XML = None
SWNDA_SC_JSON = None
# ########################################################################### #



# CMS SAM Worker Node definitions:
class swnda:
    unknown = -1
    ok = 0
    warning = 1
    error = 2
    critical = 3

    @staticmethod
    def code2name(code):
        names = ["ok", "warning", "error", "critical"]
        if (( code >= 0 ) and ( code < len(names) )):
            return names[code]
        else:
            return "unknown"

    @staticmethod
    def code2sam(code):
        samRC = [ 0, 1, 2, 2 ]
        if (( code >= 0 ) and ( code < len(samRC) )):
            return samRC[code]
        else:
            return 3

    @staticmethod
    def code2psst(code):
        psstRC = [ 0, 0, 0, 1 ]
        if (( code >= 0 ) and ( code < len(psstRC) )):
            return psstRC[code]
        else:
            return 0


    @staticmethod
    def log(level, indent, message):
        levelNames = { 'C': "Critical", 'E': "Error", 'W': "Warning",
                       'N': "Notice",   'I': "Info",  'D': "Debug",
                       'X': "X-Debug",  '?': "Unknown" }
        #
        msg = None
        try:
            level = levelNames[level][0]
        except:
            level = '?'
        try:
            indent = min( 8, max(0, int(indent)))
        except:
            indent = 0
        newline = 0
        while( message[0] == "\n" ):
            message = message[1:]
            newline += 1
        trailing = 0
        while( message[-1] == "\n" ):
            message = message[:-1]
            trailing += 1
        #
        if ( SWNDA_VERBOSITY >= 2 ):
            if ( level not in "CEWNIDX" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += ( "   " * indent ) \
                           + time.strftime("%H:%M:%S",
                                           time.gmtime(time.time())) \
                           + ( " [%s] " % level ) + line
                else:
                    msg += "\n" + ( "   " * indent ) \
                           + "        " \
                           + ( " [%s] " % level.lower() ) + line
                firstLine = False
        elif ( SWNDA_VERBOSITY == 1 ):
            if ( level not in "CEWNI" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += ( "   " * indent ) + line
                else:
                    msg += "\n" + ( "   " * indent ) + line
                firstLine = False
        elif ( SWNDA_VERBOSITY == 0 ):
            if ( level not in "CEW" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += ( "[%s] " % level ) + line
                else:
                    msg += "\n" + ( "[%s] " % level.lower() ) + line
                firstLine = False
        else:
            if ( level not in "CE" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += time.strftime("%Y-%b-%d %H:%M:%S",
                                         time.gmtime(time.time())) \
                           + ( " %s: " % levelNames[level] ) + line
                else:
                    msg += "\n                       " \
                           + ( " " * len(levelNames[level]) ) + line
                firstLine = False
        msg += "\n" * trailing
        #
        print( msg )
# ########################################################################### #



class SamWorkerNodeDataAccessAbortException(Exception):
    """Exception to break out of sequential program execution"""
    pass
# ########################################################################### #



def swnda_timeout_handler(signum, frame):
    raise TimeoutError("Signal %d caught" % signum)
    return
# ########################################################################### #



def swnda_environment():
    """print environmental variable information relevant for data-access"""


    # print environmental variables relevant for data-access:
    # =======================================================
    for myVar in [ "PATH", "LD_PRELOAD", "LD_LIBRARY_PATH", \
                   "PYTHONPATH", "PYTHON3PATH", \
                   "CMS_PATH", "SITECONFIG_PATH" ]:
        try:
            swnda.log("I", 1, "%s: %s" % (myVar, os.environ[myVar]))
        except KeyError:
            pass
    #
    varList = []
    for myVar in os.environ:
        if (( "GFAL" in myVar ) or
            ( "XRD_" in myVar ) or ( "Xrd" in myVar )):
            varList.append(myVar)
    for myVar in varList:
        if ( "GFAL" in myVar ):
            swnda.log("I", 1, "%s: %s" % (myVar, os.environ[myVar]))
    for myVar in varList:
        if (( "XRD_" in myVar ) or ( "Xrd" in myVar )):
            swnda.log("I", 1, "%s: %s" % (myVar, os.environ[myVar]))
    del varList


    return swnda.ok
# ########################################################################### #



def swnda_osidentifier():
    """identify OS and return standardized OS label"""
    global SWNDA_OSID
    #
    floatRegex = re.compile(r"^([1-9]+[0-9]*)[.]*[0-9]*$")


    # get machine architecture:
    # =========================
    osArch = "unknown"
    try:
        osArch = platform.machine()
    except Exception as excptn:
        swnda.log("E", 1, "determining machine architecture failed: %s" %
                                                                   str(excptn))


    # try /etc/os-release
    # ===================
    osSlbl = osDesc = None
    try:
        osName = osVrsn = osFmly = None
        #
        with open("/etc/os-release", 'r') as myFile:
            myLines = myFile.readlines()
        swnda.log("I", 1, "found /etc/os-release")
        #
        for myLine in myLines:
            if ( "=" not in myLine ):
                continue
            osVar = myLine.strip().split("=")[0]
            osVal = myLine.strip().split("=")[1]
            if (( osVal[0] == osVal[-1] ) and ( osVal[0] in "\"\'" )):
                osVal = osVal[1:-1]
            #
            if ( osVar == "NAME" ):
                osName = osVal
            elif ( osVar == "VERSION_ID" ):
                matchObj = floatRegex.match( osVal )
                if ( matchObj is not None ):
                    osVrsn = matchObj[1]
            elif ( osVar == "ID_LIKE" ):
                if ( "rhel" in osVal.split() ):
                    osFmly = "rhel"
                else:
                    osFmly = osVal.split()[0]
            elif ( osVar == "PRETTY_NAME" ):
                osDesc = osVal
        # analyse:
        if (( osFmly is not None ) and ( osVrsn is not None )):
            osSlbl = osFmly + osVrsn.split(".")[0]
        elif (( osName is not None ) and ( osVrsn is not None )):
            if (( osName[:18] == "Red Hat Enterprise" ) or
                ( osName[:6] == "CentOS" ) or
                ( osName[:4] == "Alma" ) or
                ( osName[:5] == "Rocky" )):
                osSlbl = "rhel" + osVrsn.split(".")[0]
        if ( osDesc is None ):
            if (( osName is not None ) and ( osVrsn is not None )):
                osDesc = osName + " " + osVrsn
            elif ( osName is not None ):
                osDesc = osName + " ?"
            elif ( osVrsn is not None ):
                osDesc = "Unknown OS " + osVrsn
            else:
                osDesc = "Unknown OS ?"
        #
        if ( osSlbl is not None ):
            SWNDA_OSID = (osArch, osSlbl)
            swnda.log("N", 1, "OS identified as %s/%s, %s" %
                                                      (osArch, osSlbl, osDesc))
            return swnda.ok
    except Exception as excptn:
        swnda.log("E", 1, "reading/interpreting /etc/os-release failed: %s" %
                                                                   str(excptn))


    # check for RedHat Enterprise Linux release files:
    # ================================================
    osSlbl = osDesc = None
    for releaseFile in ["/etc/redhat-release", "/etc/centos-release", \
                        "/etc/almalinux-release", "/etc/rocky-release" ]:
        try:
            with open(releaseFile, 'r') as myFile:
                myLine = myFile.readline()
            swnda.log("I", 1, "found %s" % releaseFile)
            #
            osDesc = myLine.strip()
            for osVal in osDesc.split():
                matchObj = floatRegex.match( osVal )
                if ( matchObj is not None ):
                    osSlbl = "rhel" + matchObj[1]
            #
            if ( osSlbl is not None ):
                SWNDA_OSID = (osArch, osSlbl)
                swnda.log("N", 1, "OS identified as %s/%s, %s" %
                                                      (osArch, osSlbl, osDesc))
                return swnda.ok
        except Exception as excptn:
            swnda.log("E", 1, "reading/interpreting %s failed: %s" %
                                                     (releaseFile,str(excptn)))


    # try /etc/SuSE-release
    # =====================
    osSlbl = osDesc = None
    try:
        osName = osVrsn = osFmly = None
        #
        with open("/etc/SuSE-release", 'r') as myFile:
            myLine = myFile.readline()
        swnda.log("I", 1, "found /etc/SuSE-release")
        #
        osDesc = myLine.strip()
        floatRegex = re.compile(r"^([1-9]+[0-9]*)[.]*[0-9]*$")
        for osVal in osDesc.split():
            matchObj = floatRegex.match( osVal )
            if ( matchObj is not None ):
                osSlbl = "suse" + matchObj[1]
        #
        if ( osSlbl is not None ):
            SWNDA_OSID = (osArch, osSlbl)
            swnda.log("N", 1, "OS identified as %s/%s, %s" %
                                                      (osArch, osSlbl, osDesc))
            return swnda.ok
    except Exception as excptn:
        swnda.log("E", 1, "reading/interpreting /etc/SuSE-release failed: %s" %
                                                                   str(excptn))
        pass


    SWNDA_OSID = (osArch, "unknown")
    swnda.log("E", 1, "OS not identified, %s/unknown", osArch)
    return swnda.critical
# ########################################################################### #



def swnda_x509setup():
    """check/setup x509 authentication environment"""
    #
    returnCode = swnda.ok


    # locate x509 certificate:
    # ========================
    if ( "X509_USER_PROXY" in os.environ ):
        swnda.log("I", 1, "found X509_USER_PROXY set")
    else:
        x509File = "/tmp/x509up_u" + str(os.geteuid())
        if ( os.path.isfile(x509File) and os.access(x509File, os.R_OK) ):
            os.environ['X509_USER_PROXY'] = x509File
            x509Name = pwd.getpwuid(os.getuid()).pw_name
            swnda.log("N", 1, "using effective uid x509 file %s of %s" %
                                                           (x509File,x509Name))
        else:
            swnda.log("C", 1, "No x509 credentials accessible")
            return swnda.critical


    # check x509 certificate is valid:
    # ================================
    now = int( time.time() )
    try:
        x509File = os.environ['X509_USER_PROXY']
        with open(x509File, "rb") as fd:
            cert = fd.read()
        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM,
                                                                          cert)
        sbjct = "".join( [ "/%s=%s" % (c[0].decode(), c[1].decode()) \
                               for c in x509.get_subject().get_components() ] )
        swnda.log("N", 1, "CMS certificate of %s" % sbjct)
        tTpl = time.strptime(x509.get_notAfter().decode('ascii'),
                                                               '%Y%m%d%H%M%SZ')
        swnda.log("I", 2, "valid until %s" % \
                                      time.strftime("%Y-%m-%d %H:%M:%S", tTpl))
        if ( int( calendar.timegm(tTpl) ) < ( now + 300 ) ):
            swnda.log("E", 2, "Expired CMS certificate, %s" %
                                      time.strftime("%Y-%m-%d %H:%M:%S", tTpl))
            returnCode = swnda.error
        vomsFlag = False
        for i in range( x509.get_extension_count() ):
            if ( x509.get_extension(i).get_short_name() == b"UNDEF" ):
                extBytes = x509.get_extension(i).get_data()
                pAuth = extBytes.find(b"cms://")
                while ( pAuth >= 0 ):
                    try:
                        indx = extBytes.find(b":", pAuth + 6)
                        vomsSrv = extBytes[pAuth + 6 : indx]
                        if (( vomsSrv == b"voms2.cern.ch" ) or
                            ( vomsSrv == b"lcg-voms2.cern.ch" ) or
                            ( vomsSrv == b"voms-cms-auth.app.cern.ch" )):
                            swnda.log("I", 2, "CMS VOMS extension issued by %s"
                                                     % vomsSrv.decode("utf-8"))
                            vomsFlag = True
                            sLen = extBytes[pAuth-1]
                            #
                            aLen = extBytes[pAuth+sLen+2]
                            atrBytes = extBytes[pAuth+sLen+3:\
                                                             pAuth+sLen+3+aLen]
                            indx = 0
                            while ( indx < len(atrBytes) ):
                                sLen = atrBytes[indx + 1]
                                swnda.log("I", 3, "%s" % atrBytes[indx+2: \
                                                  indx+2+sLen].decode("utf-8"))
                                indx += 2 + sLen
                    except:
                        pass
                    pAuth = extBytes.find(b"cms://", pAuth + 6)
        if ( vomsFlag == False ):
            swnda.log("W", 1, "seems to have no CMS VOMS extension, expect" + \
                                                       " access test failures")
            returnCode = swnda.warning
        del vomsFlag, tTpl, sbjct, x509, cert
    except Exception as excptn:
        swnda.log("E", 1, "CMS x509 certificate reading/decoding failed: %s" %
                                                                   str(excptn))
        returnCode = swnda.error


    return returnCode
# ########################################################################### #



def swnda_iamtoken():
    """check/setup x509 IAM-issued token environment"""
    #
    returnCode = swnda.ok


    # locate token:
    # =============
    if ( "BEARER_TOKEN" in os.environ ):
        swnda.log("I", 1, "found BEARER_TOKEN set")
    elif (( "BEARER_TOKEN_FILE" in os.environ ) and
          ( os.path.isfile(os.environ['BEARER_TOKEN_FILE']) ) and
          ( os.access(os.environ['BEARER_TOKEN_FILE'], os.R_OK) )):
        swnda.log("I", 1, "found BEARER_TOKEN_FILE set")
        with open(os.environ['BEARER_TOKEN_FILE'], "r") as fd:
            tokn = fd.read().strip()
        os.environ['BEARER_TOKEN'] = tokn
    elif (( "XDG_RUNTIME_DIR" in os.environ ) and
          ( os.path.isfile(os.environ['XDG_RUNTIME_DIR'] + "/bt_u" + \
                                                       str(os.geteuid())) ) and
          ( os.access(os.environ['XDG_RUNTIME_DIR'] + "/bt_u" + \
                                                str(os.geteuid()), os.R_OK) )):
        swnda.log("I", 1, "found XDG_RUNTIME_DIR set")
        with open(os.environ['XDG_RUNTIME_DIR'] + "/bt_u" + str(os.geteuid()),
                                                                    "r") as fd:
            tokn = fd.read().strip()
        os.environ['BEARER_TOKEN'] = tokn
    elif (( os.path.isfile("/tmp/bt_u" + str(os.geteuid())) ) and
          ( os.access("/tmp/bt_u" + str(os.geteuid()), os.R_OK) )):
        swnda.log("I", 1, "found /tmp/bt_u%s file" % str(os.geteuid()))
        with open("/tmp/bt_u" + str(os.geteuid()), "r") as fd:
            tokn = fd.read().strip()
        os.environ['BEARER_TOKEN'] = tokn
    else:
        swnda.log("W", 1, "No IAM-issued token accessible")
        return swnda.warning


    # clear lower priority token discovery settings (to avoid confusion):
    # ===================================================================
    try:
        del os.environ['BEARER_TOKEN_FILE']
    except KeyError:
        pass
    try:
        del os.environ['XDG_RUNTIME_DIR']
    except KeyError:
        pass


    # check token is valid:
    # =====================
    try:
        tokn = os.environ['BEARER_TOKEN']
        tknBytes = base64.urlsafe_b64decode( tokn.strip().split(".")[1] + "==" )
        tknJson = json.loads( tknBytes.decode("utf-8") )
        if ( tknJson['iss'] == "https://cms-auth.web.cern.ch/" ):
            swnda.log("N", 1, "CMS IAM-issued token, %s...%s" %
                                                         (tokn[:8], tokn[-8:]))
            swnda.log("I", 2, "JWT id  : %s" % tknJson['jti'])
            swnda.log("I", 2, "Subject : %s" % tknJson['sub'])
            try:
                swnda.log("I", 2, "ClientID: %s" % tknJson['client_id'])
            except:
                pass
            swnda.log("I", 2, "audience: %s" % tknJson['aud'])
            swnda.log("I", 2, "scope   : %s" % tknJson['scope'].split(" ")[0])
            for scpe in tknJson['scope'].split(" ")[1:]:
                swnda.log("I", 2, "          %s" % scpe)
            swnda.log("I", 2, "valid   : %s to %s" %
                              (time.strftime("%Y-%b-%d %H:%M:%S",
                                                  time.gmtime(tknJson['nbf'])),
                               time.strftime("%Y-%b-%d %H:%M:%S",
                                                 time.gmtime(tknJson['exp']))))
            if ( tknJson['nbf'] > now ):
                swnda.log("E", 2, "Unbegun CMS read-write token!")
                returnCode = swnda.error
            elif ( tknJson['exp'] < ( now + 300 ) ):
                swnda.log("E", 2, "Expired CMS read-write token")
                returnCode = swnda.error
            try:
                swnda.log("I", 2, "issued  : %s" %
                                             time.strftime("%Y-%b-%d %H:%M:%S",
                                                  time.gmtime(tknJson['iat'])))
            except:
                pass
            for key in tknJson:
                if key in ["iss", "jti", "sub", "client_id", "aud",
                           "scope", "nbf", "exp", "iat"]:
                    continue
                swnda.log("I", 2, "%-8s: %s" % (str(key), str(tknJson[key])))
        else:
            swnda.log("E", 1, "Not a CMS token, %s...%s" %
                                                         (tokn[:8], tokn[-8:]))
            try:
                swnda.log("E", 2, "Issuer  : %s" % tknJson['iss'])
            except:
                pass
            returnCode = swnda.error
        del tknJson, tknBytes, tokn
    except Exception as excptn:
        swnda.log("E", 1, "CMS IAM-issued token reading/decoding failed: %s" %
                                                                   str(excptn))
        returnCode = swnda.error


    return returnCode
# ########################################################################### #



def swnda_source(shScript):
    """set/update environment variables set/changed by a shell script"""

    swnda.log("N", 1, "Sourcing %s:" % shScript)


    # check file exists:
    # ==================
    if ( os.path.isfile(shScript) != True ):
        swnda.log("E", 1, "script %s does not exist" % shScript)
        return swnda.error


    # source script and print environment variables:
    # ==============================================
    try:
        myCmd = "/bin/sh -c \". %s && printenv -0\"" % shScript
        for myLine in subprocess.getoutput(myCmd).split("\0"):
            myVar, myVal = ( myLine.split("=",1) + [""] )[:2]
            # set environment variables in current/python process:
            # ====================================================
            swnda.log("D", 1, "setting %s to \"%s\"" % (myVar, myVal))
            os.environ[myVar] = myVal
    except Exception as excptn:
        swnda.log("E", 1, ("sourcing script and updating environment faile" + \
                                                        "d, %s") % str(excptn))
        return swnda.error


    return swnda.ok
# ########################################################################### #



def swnda_cmssetup():
    """setup CMS environment, a la GitHub:dmwm/WMCore/etc/submit_py3.sh"""
    #
    returnCode = swnda.ok


    try:
        # start with old LCG location check:
        # ==================================
        setupFile = os.environ.get("VO_CMS_SW_DIR", "") + "/cmsset_default.sh"
        if ( os.path.isfile( setupFile ) ):
            swnda.log("I", 1, "found CMS setup file at LCG location")
        else:
            setupFile = os.environ.get("OSG_APP", "") + \
                                               "/cmssoft/cms/cmsset_default.sh"
            if ( os.path.isfile( setupFile ) ):
                swnda.log("I", 1, "found CMS setup file at OSG location")
            else:
                setupFile = os.environ.get("CVMFS", "") + \
                                               "/cms.cern.ch/cmsset_default.sh"
                if ( os.path.isfile( setupFile ) ):
                    swnda.log("I", 1, "found CMS setup file at CVMFS location")
                else:
                    setupFile = "/cvmfs/cms.cern.ch/cmsset_default.sh"
                    if ( os.path.isfile( setupFile ) ):
                        swnda.log("I", 1, "using CMS setup file from defau" + \
                                                           "lt CVMFS location")
                    else:
                        swnda.log("C", 1, "no CMS setup file found")
                        return swnda.critical


        # source setup file:
        # ==================
        returnCode = swnda_source( setupFile )


    except Exception as excptn:
        swnda.log("E", 1, "locating and sourcing CMS setup file failed: %s" %
                                                                   str(excptn))
        returnCode = swnda.error


    return returnCode
# ########################################################################### #



def swnda_siteconf():
    """examine SITECONF area and compile XML and JSON-based data-access list"""
    global SWNDA_SC_SITE
    global SWNDA_SC_SUBSITE
    global SWNDA_SC_XML
    global SWNDA_SC_JSON
    #
    tfcRegex = re.compile(
                       r".*trivialcatalog_file:([^:?]+)\?.*protocol=([^&]+).*")
    #
    returnCode = swnda.ok


    # we are after a cmsset_default.sh, SITECONFIG_PATH should be set:
    # ================================================================
    try:
        # resolve symbolic links:
        scPath = os.path.realpath( os.environ['SITECONFIG_PATH'] )
        if ( scPath[-1] != "/" ):
            scPath += "/"


        # examine JobConfig/site-local-config.xml:
        # ========================================
        slcFile = scPath + "JobConfig/site-local-config.xml"
        if ( os.path.isfile( slcFile ) == False ):
            swnda.log("C", 1, "No JobConfig/site-local-config.xml file")
            return swnsc.critical
        #
        with open( slcFile, 'r') as myFile:
            slcData = myFile.read()
        #
        slcXML = xml.etree.ElementTree.fromstring( slcData )
        #
        siteElemnt = slcXML.find("site")
        SWNDA_SC_SITE = siteElemnt.attrib['name']
        try:
            SWNDA_SC_SUBSITE = siteElemnt.find("subsite").attrib['name']
        except:
            SWNDA_SC_SUBSITE = ""
        #
        # PhEDEx/storage.xml based access:
        # --------------------------------
        SWNDA_SC_XML = set()
        try:
            catTags = siteElemnt.find("event-data").findall("catalog")
            if ( len(catTags) > 0 ):
                for myTag in catTags:
                    matchObj = tfcRegex.match( myTag.attrib['url'] )
                    if ( matchObj is None ):
                        swnda.log("W", 1, "Bad <catalog> tag in <event-dat" + \
                               "a> section of JobConfig/site-local-config.xml")
                        if ( returnCode == swnda.ok ):
                            returnCode = swnda.warning
                        continue
                    SWNDA_SC_XML.add( (matchObj[1], matchObj[2]) )
            else:
                swnda.log("E", 1, "No <catalog> tag in <event-data> sectio" + \
                                        "n of JobConfig/site-local-config.xml")
                returnCode = swnda.error
        except Exception as excptn:
            swnda.log("E", 1, ("Error finding PhEDEx/storage.xml based dat" + \
              "a access in JobConfig/site-local-config.xml, %s") % str(excptn))
            returnCode = swnda.error
        #
        # storage.json based access:
        # --------------------------
        SWNDA_SC_JSON = set()
        try:
            catTags = siteElemnt.find("data-access").findall("catalog")
            if ( len(catTags) > 0 ):
                for myTag in catTags:
                    try:
                        mySite = myTag.attrib['site']
                    except KeyError:
                        mySite = SWNDA_SC_SITE
                    try:
                        myVolm = myTag.attrib['volume']
                        myProt = myTag.attrib['protocol']
                    except KeyError:
                        swnda.log("W", 1, "Bad <catalog> tag in <data-acce" + \
                              "ss> section of JobConfig/site-local-config.xml")
                        if ( returnCode == swnda.ok ):
                            returnCode = swnda.warning
                        continue
                    SWNDA_SC_JSON.add( (mySite, myVolm, myProt) )
            else:
                swnda.log("E", 1, "No <catalog> tag in <data-access> secti" + \
                                       "on of JobConfig/site-local-config.xml")
                returnCode = swnda.error
        except Exception as excptn:
            swnda.log("E", 1, ("Error finding storage.json based data acce" + \
                    "ss in JobConfig/site-local-config.xml, %s") % str(excptn))
            returnCode = swnda.error


    except Exception as excptn:
        swnda.log("E", 1, "SITECONF area examination failed: %s" % str(excptn))
        returnCode = swnda.error


    if ((( SWNDA_SC_XML is None ) and ( SWNDA_SC_JSON is None )) or
        (( SWNDA_SC_XML is None ) and ( SWNDA_SC_JSON is not None ) and
                                      ( len(SWNDA_SC_JSON) == 0 )) or
        (( SWNDA_SC_XML is not None ) and
         ( len( SWNDA_SC_XML ) == 0 ) and ( SWNDA_SC_JSON is None ))):
        # require at least either XML or JSON data access:
        returnCode = swnda.critical


    return returnCode
# ########################################################################### #



def swnda_cmsrun(cmsswTpl, lfnFile):
    """run a data-access to lfnFile using CMSSW of cmsswTpl"""
    #
    returnCode = swnda.ok


    try:
        #
        # create a CMSSW release area:
        myCmd = [ "scram", "-a", cmsswTpl[1], "project", "CMSSW", cmsswTpl[0] ]
        cmpProc = subprocess.run(myCmd, stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT, timeout=90)
        if ( cmpProc.returncode != 0 ):
            swnda.log("E", 1, "Error creating CMSSW release area:\n%s\n%s" %
                                         (str(myCmd), cmpProc.stdout.decode()))
            return swnda.error


        # change into CMSSW release area:
        os.chdir(cmsswTpl[0])
        #
        # loop over JobConfig/site-local-config.xml data-access list:
        if ( cmsswTpl[2] == "xml" ):
            daList = list( SWNDA_SC_XML )
        elif ( cmsswTpl[2] == "json" ):
            daList = list( SWNDA_SC_JSON )
        else:
            os.chdir("..")
            return swnda.unknown
        daSuccess = 0
        for daItem in daList:
            #
            # write cmsRun config file:
            if ( cmsswTpl[2] == "xml" ):
                ocStrng = "trivialcatalog_file:%s?protocol=%s" % \
                                                         (daItem[0], daItem[1])
            elif ( cmsswTpl[2] == "json" ):
                ocStrng = "%s,%s,%s,%s,%s" % (SWNDA_SC_SITE, SWNDA_SC_SUBSITE,
                                               daItem[0], daItem[1], daItem[2])
            #
            swnda.log("N", 1, "data-access: %s" % ocStrng)
            #
            #
            with open("swnda_cfg.py", 'w') as myFile:
                myFile.write(("#\nimport FWCore.ParameterSet.Config as cms" + \
                              "\nprocess = cms.Process(\"SAMtest\")\nproce" + \
                              "ss.source = cms.Source('PoolSource',\n   ov" + \
                              "errideCatalog = cms.untracked.string(\"%s\"" + \
                              "),\n   fileNames = cms.untracked.vstring(\"" + \
                              "%s\")\n)\n\nprocess.SiteLocalConfigService " + \
                              "= cms.Service(\"SiteLocalConfigService\",\n" + \
                              "   debugLevel = cms.untracked.uint32(1),\n " + \
                              "  overrideSourceCacheHintDir = cms.untracke" + \
                              "d.string(\"application-only\"),\n)\n\nproce" + \
                              "ss.dump = cms.EDAnalyzer(\"EventContentAnal" + \
                              "yzer\",\n   listContent = cms.untracked.boo" + \
                              "l(False),\n   getData = cms.untracked.bool(" + \
                              "True)\n)\nprocess.load(\"FWCore.MessageServ" + \
                              "ice.MessageLogger_cfi\")\nprocess.MessageLo" + \
                              "gger.cerr.FwkReport.reportEvery = 1\n\nproc" + \
                              "ess.maxEvents = cms.untracked.PSet(\n   inp" + \
                              "ut = cms.untracked.int32(0)\n)\n\nprocess.p" + \
                              " = cms.EndPath(process.dump)") % \
                                                             (ocStrng,lfnFile))
            #
            #
            # setup CMSSW environment and execute cmsRun:
            myCmd = [ "/bin/sh", "-c", \
                              "eval `scram runtime -sh`; cmsRun swnda_cfg.py" ]
            cmpProc = subprocess.run(myCmd, stdout=subprocess.PIPE,
                                         stderr=subprocess.STDOUT, timeout=180)
            if ( cmpProc.returncode == 0 ):
                swnda.log("N", 2, "==> ok")
                daSuccess += 1
            else:
                swnda.log("E", 2, ("Error setting up CMSSW and executing c" + \
                     "msRun:\n%s\n%s") % (str(myCmd), cmpProc.stdout.decode()))
        #
        # cd out of CMSSW release area:
        os.chdir("..")
        #
        #
        # evaluate data-access results:
        if ( daSuccess == 0 ):
            returnCode = swnda.error
        elif ( daSuccess < len( daList ) ):
            returnCode = swnda.warning

    except Exception as excptn:
        swnda.log("E", 1, "setup of %s and/or cmsRun failed: %s" %
                                                    (cmsswTpl[0], str(excptn)))
        returnCode = swnda.critical

    return returnCode
# ########################################################################### #



if __name__ == '__main__':
    #
    try:
        import signal
        #
        #
        #
        parserObj = argparse.ArgumentParser(description="CMS SAM WorkerNode " +
                                                           "data-access probe")
        parserObj.add_argument("-S", dest="sam", action="store_true",
                                     default=False,
                                     help="ETF configuration, SAM mode")
        parserObj.add_argument("-P", dest="psst", action="store_true",
                                     default=False,
                                     help="fast, minimal-output probing, PSS" +
                                                                      "T mode")
        parserObj.add_argument("-t", dest="timeout", type=int, default=300,
                                     help="maximum time probe is allowed to " +
                                                       "take in seconds [300]")
        parserObj.add_argument("-v", dest="verbose", action="count",
                                     help="increase verbosity")
        argStruct = parserObj.parse_args()
        #
        #
        #
        if ( argStruct.sam ):
            SWNDA_VERBOSITY = 1
        elif ( argStruct.psst ):
            SWNDA_VERBOSITY = -1
        if ( argStruct.verbose is not None ):
            SWNDA_VERBOSITY = argStruct.verbose
        #
        #
        #
        # set timeout:
        start_tis = time.time()
        signal.signal(signal.SIGALRM, swnda_timeout_handler)
        signal.alarm( argStruct.timeout )
        #
        #
        #
        # initialize to ok:
        summaryMSG = "data-access check ok"
        statusCode = swnda.ok
        swnda.log("N", 0, "Starting CMS data-access check of %s on %s UTC" %
                          (socket.gethostname(),
                   time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(start_tis))))
        swnda.log("N", 1, "WN-x5dataaccess version %s" % SWNDA_VERSION)
        swnda.log("N", 2, "python version %d.%d.%d\n" % sys.version_info[0:3])
        #
        #
        #
        # check/print environment settings relevant for data-access:
        swnda.log("N", 0, "\nInitial environment:")
        swnda_environment()
        #
        #
        # identify OS version:
        # ===============
        swnda.log("N", 0, "\nIdentifying OS:")
        statusCode = swnda_osidentifier()
        if ( statusCode == swnda.critical ):
            summaryMSG = "critical OS identification failure"
            raise SamWorkerNodeDataAccessAbortException("OS identification")
        #
        if ( SWNDA_OSID not in SWNDA_CMSSW_LIST ):
            swnda.log("N", 0, "No CMSSW configured for %s/%s data access" %
                                                 (SWNDA_OSID[0],SWNDA_OSID[1]))
            statusCode = swnda.warning()
            raise SamWorkerNodeDataAccessAbortException("CMSSW config")
        #
        #
        # check/setup x509 environment:
        # =============================
        swnda.log("N", 0, "\nCheck/setup of x509 authentication:")
        statusCode = swnda_x509setup()
        if ( statusCode == swnda.critical ):
            summaryMSG = "critical x509 certificate failure"
            raise SamWorkerNodeDataAccessAbortException("x509 certificate")
        #
        #
        # check/setup IAM-issued token environment:
        # =========================================
        swnda.log("N", 0, "\nCheck/setup of IAM-issued token authentication:")
        statusCode = swnda_iamtoken()
        if ( statusCode == swnda.critical ):
            summaryMSG = "critical IAM-issued token failure"
            raise SamWorkerNodeDataAccessAbortException("IAM issued token")
        #
        #
        # setup CMS environment:
        # ======================
        swnda.log("N", 0, "\nCheck/setup of CMS environment:")
        statusCode = swnda_cmssetup()
        if ( statusCode == swnda.critical ):
            summaryMSG = "critical RCMS setup failure"
            raise SamWorkerNodeDataAccessAbortException("CMS setup")
        #
        #
        # read SITECONF and get storage.xml and storage.json data-access:
        # ===============================================================
        swnda.log("N", 0, "\nFetching data-access information from SITECONF:")
        statusCode = swnda_siteconf()
        swnda.log("N", 1, "==> %s" % swnda.code2name(statusCode) )
        if ( statusCode == swnda.critical ):
            summaryMSG = "critical SITECONF access failure"
            raise SamWorkerNodeDataAccessAbortException("SITECONF")
        #
        #
        # create a subdirectory for our test and cd into it:
        # ==================================================
        iniDir = os.getcwd()
        swnda.log("N", 0, "\nCreating \"swnda\" subdirectory in %s:" % iniDir)
        if ( os.path.exists("swnda") ):
            swnda.log("W", 1, "removing existing \"swnda\" path")
            shutil.rmtree("swnda")
        os.mkdir("swnda", mode=0o755)
        # cd into it:
        os.chdir("swnda")
        tstDir = os.getcwd()
        #
        #
        # pick a file in the SAM dataset:
        indxFile = int( len(SWNDA_FILES) * random.random() )
        #
        for tstCMSSW in SWNDA_CMSSW_LIST[SWNDA_OSID]:
            #
            #
            # check data-access in cmsRun of CMSSW version:
            swnda.log("N", 0, "\nChecking data-access in %s:" % tstCMSSW[0])
            status = swnda_cmsrun( tstCMSSW, SWNDA_FILES[indxFile] )
            if (( status == swnda.warning ) and ( statusCode == swnda.ok )):
                statusCode = swnda.warning
                summaryMSG = "warning %s cmsRun issue" % tstCMSSW[0]
            elif (( status == swnda.unknown ) and
                  (( statusCode == swnda.ok ) or
                   ( statusCode == swnda.warning ))):
                statusCode = swnda.unknown
                summaryMSG = "unknown %s cmsRun issue" % tstCMSSW[0]
            elif (( status == swnda.error ) and
                  (( statusCode == swnda.ok ) or
                   ( statusCode == swnda.warning ) or
                   ( statusCode == swnda.unknown ))):
                statusCode = swnda.error
                summaryMSG = "error %s cmsRun issue" % tstCMSSW[0]
            elif (( status == swnda.critical ) and
                  (( statusCode == swnda.ok ) or
                   ( statusCode == swnda.warning ) or
                   ( statusCode == swnda.error ))):
                statusCode = swnda.critical
                summaryMSG = "critical %s cmsRun issue" % tstCMSSW[0]
                break
            #
            #
            # use next file in SAM dataset
            indxFile = ( indxFile + 1 ) % len(SWNDA_FILES)
        #
        #
        # change back to initial directory and delete test subdirectory:
        swnda.log("N", 0, "\nRemoving \"swnda\" subdirectory:")
        os.chdir(iniDir)
        shutil.rmtree("swnda")

    except SamWorkerNodeDataAccessAbortException:
        pass

    except TimeoutError as excptn:
        delta = time.time() - start_tis
        swnda.log("E", 0, "Maximum execution time exceeded, %.3f sec" % delta)
        statusCode = swnda.error
        #
        if ( argStruct.sam ):
            print("\nExecution timeout\n")
            sys.exit(swnda.code2sam(statusCode))
        elif ( argStruct.psst ):
            sys.exit(swnda.code2psst(statusCode))
        else:
            sys.exit(statusCode)


    except Exception as excptn:
        swnda.log("E", 0, "Execution failed, %s" % str(excptn))
        statusCode = swnda.error
        #
        swnda.log("E", 0, traceback.format_exc() )
        #
        if ( argStruct.sam ):
            print("\nExecution failure\n")
            sys.exit(swnda.code2sam(statusCode))
        elif ( argStruct.psst ):
            sys.exit(swnda.code2psst(statusCode))
        else:
            sys.exit(statusCode)

    if ( argStruct.sam ):
        print("\n%s\n" % summaryMSG)
        sys.exit(swnda.code2sam(statusCode))
    elif ( argStruct.psst ):
        sys.exit(swnda.code2psst(statusCode))
    else:
        sys.exit(statusCode)
    #import pdb; pdb.set_trace()
