#!/bin/sh
# ########################################################################### #
#
# SAM WorkerNode basic probe of CMS
#
# ########################################################################### #
# cancel timeout_handler if alive:
trap '(/usr/bin/ps -p ${ALARM_PID}; if [ $? -eq 0 ]; then kill -ABRT ${ALARM_PID}; fi) 1>/dev/null 2>&1' 0
# trap SIGALRM to handle timeouts properly:
trap 'if [ -n "${SAM_MODE}" ]; then echo -e "\nExecution timeout\n"; exit 2; elif [ -n "${PSST_MODE}" ]; then exit 0; else exit 3; fi' 14
#
#
#
SWN_VERSION="v1.00.00"
#
#
#
SWN_OK=0
SWN_WARNING=1
SWN_UNKNOWN=2
SWN_ERROR=3
SWN_CRITICAL=4
#
swn2sam () {
   if [ $1 -eq 0 ]; then
      return 0
   elif [ $1 -eq 1 ]; then
      return 1
   elif [ $1 -eq 3 -o $1 -eq 4 ]; then
      return 2
   else
      return 3
   fi
}
#
swn2psst () {
   if [ $1 -eq 4 ]; then
      return 1
   else
      return 0
   fi
}
#
timeout_handler () {
   sleep ${TIMEOUT}
   kill -ALRM $1
}
#
SWNRC=${SWN_OK}
SUMMRY="Basic check ok"
# ########################################################################### #



# handle command line options, setup timeout, and print OS:
SAM_MODE=""
PSST_MODE=""
VERBOSE=0
TIMEOUT=60
IARG=1
while [ ${IARG} -le $# ]; do
   case "${!IARG}" in
   -S) SAM_MODE="yes"
       ;;
   -P) PSST_MODE="yes"
       ;;
   -t) IARG=`expr ${IARG} + 1`
       TIMEOUT=${!IARG}
       ;;
   -v) VERBOSE=`expr ${VERBOSE} + 1`
       ;;
   -vv) VERBOSE=2
       ;;
   -vvv) VERBOSE=3
       ;;
   -vvvv) VERBOSE=4
       ;;
   *) echo "usage: $0 [-h] [-S] [-P] [-t TIMEOUT] [-v]"
      echo -e "\nCMS SAM WorkerNode Basic probe\n\noptional arguments:"
      echo "  -h, --help  show this help message and exit"
      echo "  -S          ETF configuration, SAM mode"
      echo "  -P          fast, minimal-output probing, PSST mode"
      echo "  -t TIMEOUT  maximum time probe is allowed to take in seconds [60]"
      echo "  -v          increase verbosity"
      exit 0
   esac
   IARG=`expr ${IARG} + 1`
done
timeout_handler $$ &
ALARM_PID=$!
#
#
TMP=`/usr/bin/date +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
echo "Starting CMS basic check on ${TMP} UTC"
echo -e "   WN-01basic version ${SWN_VERSION}\n"
unset TMP
#
/usr/bin/uname -a
if [ -r /etc/os-release ]; then
   TMP=`. /etc/os-release; echo ${PRETTY_NAME}`
   if [ -n "${TMP}" ]; then
      echo "${TMP}"
   else
      /bin/sh -c '. /etc/os-release; echo "${NAME} ${VERSION}"'
   fi
elif [ -r /etc/redhat-release ]; then
   /bin/cat /etc/redhat-release
elif [ -r /etc/centos-release ]; then
   /bin/cat /etc/centos-release
elif [ -r /etc/almalinux-release ]; then
   /bin/cat /etc/almalinux-release
elif [ -r /etc/SuSE-release ]; then
   /bin/cat /etc/SuSE-release
fi
echo ""
# ########################################################################### #



# assume commands equivalent to coreutils are available and probe any other
#   commands we need/use:
CMD_LIST="/usr/bin/ps /usr/bin/which /usr/bin/awk /usr/bin/uptime /usr/bin/hostname /usr/bin/curl /usr/bin/bc /usr/bin/attr /usr/bin/grep"
for CMD in ${CMD_LIST}; do
   if [ ! -x ${CMD} ]; then
      echo "[E] OS command \"${CMD}\" unavailable"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="Missing command"
      fi
   fi
done
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   if [ -n "${SAM_MODE}" ]; then
      SAMRC=`swn2sam ${SWNRC}`
      echo -e "\n${SUMMRY}\n"
      exit ${SAMRC}
   elif [ -n "${PSST_MODE}" ]; then
      PSSTRC=`swn2psst ${SWNRC}`
      exit ${PSSTRC}
   else
      exit ${SWNRC}
   fi
else
   echo "Required commands available"
fi
echo ""
# ########################################################################### #



# try to identify worker node environment, container, VM, machine, etc.:
if [ -n "${APPTAINER_CONTAINER}" ]; then
   echo -n -e "We seem to be running inside an Apptainer container\n   "
   MYCMD=`/usr/bin/which apptainer 2>/dev/null`
   if [ $? -eq 0 ]; then
      ${MYCMD} --version
   else
      echo "without apptainer command"
   fi
   unset MYCMD
   SWNB_ENV=Apptainer
elif [ -n "${SINGULARITY_CONTAINER}" ]; then
   echo -n -e "We seem to be running inside a Singularity container\n   "
   MYCMD=`/usr/bin/which singularity 2>/dev/null`
   if [ $? -eq 0 ]; then
      ${MYCMD} --version
   else
      echo "without singularity command"
   fi
   unset MYCMD
   SWNB_ENV=Singularity
elif [ -n "${SHIFTER}" -a -n "${SHIFTER_RUNTIME}" ]; then
   echo "We seem to be running inside a Shifter container"
   SWNB_ENV=Shifter
elif [ -f /.dockerenv ]; then
   if [ -r /proc/1/cgroup ]; then
      SWNB_ENV=`/usr/bin/awk -F: '{n=split($2,a,",");for(i=1;i<=n;i++ ){if(a[i]=="cpu"){n=split($3,b,"/");for(j=1;j<=n;j++ ){if((b[j]=="lxc")||(b[j]=="docker")){print "Docker"}}exit}}}' /proc/1/cgroup`
      if [ "${SWNB_ENV}" = "Docker" ]; then
         echo "We seem to be running inside a Docker container"
      fi
   else:
      echo "We could be running inside a Docker container"
   fi
fi
if [ -z "${SWNB_ENV}" ]; then
   SWNB_ENV=`/usr/bin/awk -F: '/^flags[ \t]*:/ {n=split($2,a," ");for(i=1;i<=n;i++ ) {if(a[i]=="hypervisor"){print "Hypervisor"}}exit}' /proc/cpuinfo`
   if [ "${SWNB_ENV}" = "Hypervisor" ]; then
      echo -n -e "We seem to be run by a Hypervisor\n   "
      if [ -e /sys/devices/virtual/dmi/id/product_name ]; then
         /usr/bin/cat /sys/devices/virtual/dmi/id/product_name
      else
         echo "no DMI product name"
      fi
   fi
fi
if [ -z "${SWNB_ENV}" ]; then
   echo "We are most likely running on a physical machine"
fi
echo ""
# ########################################################################### #



# get processor/memory configuration and check CPU load/memory utilization:
/usr/bin/awk -F: 'BEGIN{np=0}{if(substr($1,1,10)=="processor\t"){np+=1};if(substr($1,1,11)=="model name\t"){mn=$2;gsub(/^[ \t]+/,"",mn)};if(substr($1,1,8)=="cpu MHz\t"){sp=$2;gsub(/^[ \t]+/,"",sp)}}END{printf "   %d %s currently at %s MHz\n",np,mn,sp}' /proc/cpuinfo
SWNB_CPU=`/usr/bin/awk 'BEGIN{np=0}/^processor[ \t]*:/{np+=1}END{print np}' /proc/cpuinfo`
if [ -n "${SAM_MODE}" ]; then
   SWNB_SLOT=1
elif [ -n "${PSST_MODE}" ]; then
   if [ -n "${GLIDEIN_CPUS}" ]; then
      SWNB_SLOT=${GLIDEIN_CPUS}
   else
      SWNB_SLOT=8
   fi
else
   SWNB_SLOT=${SWNB_CPU}
fi
echo "Assuming ${SWNB_CPU} cores of which ${SWNB_SLOT} are for our use"
#
/usr/bin/awk 'BEGIN{mt=0;ma=0;st=0;sf=0}{if($1=="MemTotal:"){mt=$2};if($1=="MemAvailable:"){ma=$2};if($1=="SwapTotal:"){st=$2};if($1=="SwapFree:"){sf=$2}}END{printf "   Memory:   total %.3f GiB   available %.3f GiB\n   Swap:     total %.3f GiB   used %.3f GiB\n",mt/1048576,ma/1048576,st/1048576,(st-sf)/1048576}' /proc/meminfo
TMP=`/usr/bin/awk -vslot=${SWNB_SLOT} '/^MemAvailable:/{a=int($2/slot/1024);print a;exit}' /proc/meminfo`
if [ ${TMP} -lt 1000 ]; then
   echo "[C] Inusufficient memory available, ${TMP} MiB/core"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
      SWNRC=${SWN_CRITICAL}
      SUMMRY="critical low memory issue"
   fi
elif [ ${TMP} -lt 2048 ]; then
   echo "[E] Incomplete memory available, ${TMP} MiB/core"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="low memory error"
   fi
fi
#
echo -n -e "Current system load:\n   "
/usr/bin/uptime
TMP=`/usr/bin/uptime | /usr/bin/awk '{print substr($(NF-2),1,length($(NF-2))-1);exit}'`
TMP1=`echo ${TMP} | /usr/bin/awk -vcpu=${SWNB_CPU} '{print int(100*($1-1)/cpu);exit}'`
TMP2=`echo ${TMP} | /usr/bin/awk -vcpu=${SWNB_CPU} -vslot=${SWNB_SLOT} '{print int(100*($1-1+slot)/cpu);exit}'`
if [ ${TMP1} -gt 200 ]; then
   echo "[C] CPU overloaded, 1 min load ${TMP}%"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
      SWNRC=${SWN_CRITICAL}
      SUMMRY="critical CPU overload issue"
   fi
elif [ ${TMP2} -lt 67 ]; then
   echo "[W] High CPU load, expected CPU usage ${TMP2}%"
   if [ ${SWNRC} -eq ${SWN_OK} ]; then
      SWNRC=${SWN_WARNING}
      SUMMRY="high CPU load issue"
   fi
fi
unset TMP2
unset TMP1
unset TMP
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   if [ -n "${SAM_MODE}" ]; then
      SAMRC=`swn2sam ${SWNRC}`
      echo -e "\n${SUMMRY}\n"
      exit ${SAMRC}
   elif [ -n "${PSST_MODE}" ]; then
      PSSTRC=`swn2psst ${SWNRC}`
      exit ${PSSTRC}
   else
      exit ${SWNRC}
   fi
fi
echo ""
# ########################################################################### #



# find out user, account, and authentication information:
if [ -z "${PSST_MODE}" ]; then
   /usr/bin/id
   echo "#"
   #
   ulimit -a
   echo "#"
   #
   if [ -n "${X509_USER_PROXY}" ]; then
      echo "X509_USER_PROXY is set to ${X509_USER_PROXY}:"
      if [ -x /usr/bin/voms-proxy-info ]; then
         /usr/bin/voms-proxy-info -all -file ${X509_USER_PROXY}
      fi
   elif [ -r "/tmp/x509up_u`/usr/bin/id -u`" ]; then
      echo "/tmp/x509up_u`/usr/bin/id -u` exists:"
      if [ -x /usr/bin/voms-proxy-info ]; then
         /usr/bin/voms-proxy-info -all -file /tmp/x509up_u`/usr/bin/id -u`
      fi
   else
      echo "no x509 certificate"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="no certificate error"
      fi
   fi
   #
   if [ -n "${BEARER_TOKEN}" ]; then
      echo "BEARER_TOKEN is set:"
      if [ -x /usr/bin/httokendecode ]; then
         echo "${BEARER_TOKEN}" | /usr/bin/httokendecode -H -
      else
         echo "${BEARER_TOKEN}" | /usr/bin/awk \
            '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}'
      fi
   elif [ -n "${BEARER_TOKEN_FILE}" -a \
          -r "${BEARER_TOKEN_FILE}" ]; then
      echo "BEARER_TOKEN_FILE set and readable:"
      if [ -x /usr/bin/httokendecode ]; then
         /usr/bin/httokendecode -H ${BEARER_TOKEN_FILE}
      else
         /usr/bin/awk '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}' \
            ${BEARER_TOKEN_FILE}
      fi
   elif [ -n "${XDG_RUNTIME_DIR}" -a \
          -r "${XDG_RUNTIME_DIR}/bt_u`/usr/bin/id -u`" ]; then
      echo "XDG_RUNTIME_DIR set and euid-based file readable:"
      if [ -x /usr/bin/httokendecode ]; then
         /usr/bin/httokendecode -H ${XDG_RUNTIME_DIR}/bt_u`/usr/bin/id -u`
      else
         /usr/bin/awk '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}' \
            ${XDG_RUNTIME_DIR}/bt_u`/usr/bin/id -u`
      fi
   elif [ -r "/tmp/bt_u`/usr/bin/id -u`" ]; then
      echo "default euid-based file readable:"
      if [ -x /usr/bin/httokendecode ]; then
         /usr/bin/httokendecode -H /tmp/bt_u`/usr/bin/id -u`
      else
         /usr/bin/awk '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}' \
            /tmp/bt_u`/usr/bin/id -u`
      fi
   else
      echo "no IAM-issued token"
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no token warning"
      fi
   fi
   #
   echo ""
fi
# ########################################################################### #



# check available disk space:
MYPWD=`/usr/bin/pwd`
echo "Current working directory \"${MYPWD}\""
TMP=`/usr/bin/df -k ${MYPWD} | /usr/bin/awk '{if(NR>1){printf "%.3f\n",$4/1048576;exit}}'`
echo "   its filesystem has ${TMP} GiB free space"
TMP=`/usr/bin/df -k /tmp | /usr/bin/awk '{if(NR>1){printf "%.3f\n",$4/1048576;exit}}'`
echo "System /tmp filesystem has ${TMP} GiB free space"
echo ""
# ########################################################################### #



# check IPv4 and v6 connectivity to cern and system clock:
SWN_IPv4=`/usr/bin/awk 'BEGIN{c=""}{if(($2=="00000000")&&(and(strtonum("0x"$4),2)==2)){c=$1;exit}}END{print c}' /proc/net/route 2>/dev/null`
SWN_IPv6=`/usr/bin/awk 'BEGIN{c=""}{if(($1=="00000000000000000000000000000000")&&(and(strtonum("0x"$9),2)==2)){c=$10;exit}}END{print c}' /proc/net/ipv6_route 2>/dev/null`
if [ -n "${SWN_IPv4}" -a -n "${SWN_IPv6}" ]; then
   echo "Network config: dual stack"
elif [ -n "${SWN_IPv4}" -a -z "${SWN_IPv6}" ]; then
   echo "Network config: IPv4-only"
elif [ -z "${SWN_IPv4}" -a -n "${SWN_IPv6}" ]; then
   echo "Network config: IPv6-only"
else
   echo "Network config: no global IP gateway"
fi
#
if [ -z "${PSST_MODE}" ]; then
   CERNNOW=""
   if [ -n "${SWN_IPv4}" ]; then
      TMP=`/usr/bin/curl -s -k -4 --max-time 10 --head https://www.cern.ch/ 2>/dev/null`
      if [ $? -eq 0 ]; then
         MYNOW=`/usr/bin/date +'%s' 2>/dev/null`
         echo "IPv4 connectivity to CERN"
         TMP=`echo "${TMP}" | /usr/bin/awk '/^Date: .*/ {print substr($0,7)}'`
         if [ -n "${TMP}" ]; then
            echo "   timestamp in header ${TMP}"
            CERNNOW=`/usr/bin/date --date="${TMP}" +'%s'`
         else
            echo "   no timestamp in header"
         fi
      else
         echo "[E] No IPv4 connectivity to CERN"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="No IPv4 connectivity to CERN"
         fi
      fi
   fi
   #
   if [ -n "${SWN_IPv6}" ]; then
      TMP=`/usr/bin/curl -s -k -6 --max-time 10 --head https://www.cern.ch/ 2>/dev/null`
      if [ $? -eq 0 ]; then
         echo "IPv6 connectivity to CERN"
         if [ -z "${CERNNOW}" ]; then
            MYNOW=`/usr/bin/date +'%s' 2>/dev/null`
            TMP=`echo "${TMP}" | /usr/bin/awk '/^Date: .*/ {print substr($0,7)}'`
            if [ -n "${TMP}" ]; then
               echo "   timestamp in header ${TMP}"
               CERNNOW=`/usr/bin/date --date="${TMP}" +'%s'`
            else
               echo "   no timestamp in header"
            fi
         fi
      else
         echo "[W] No IPv6 connectivity to CERN"
         if [ ${SWNRC} -eq ${SWN_OK} ]; then
            SWNRC=${SWN_WARNING}
            SUMMRY="No IPv6 connectivity to CERN"
         fi
      fi
   fi
   #
   if [ -n "${CERNNOW}" ]; then
      TMP=`echo "d=${CERNNOW}-${MYNOW};if(d<0){-d}else{d}" | /usr/bin/bc`
      if [ ${TMP} -le 15 ]; then
         echo "Node and CERN time within ${TMP} seconds"
      else
         echo "Node and CERN time are off by ${TMP} seconds"
         TMP=`(/usr/bin/curl -s -k --max-time 10 --head https://www.ntppool.org/ | /usr/bin/awk '/^[Dd]ate: .*/ {print substr($0,7)}') 2>/dev/null`
         if [ $? -eq 0 ]; then
            MYNOW=`/usr/bin/date +'%s' 2>/dev/null`
            NTPNOW=`/usr/bin/date --date="${TMP}" +'%s'`
            TMP=`echo "d=${NTPNOW}-${MYNOW};if(d<0){-d}else{d}" | /usr/bin/bc`
            if [ ${TMP} -gt 900 ]; then
               echo "[E] Wrong time on the node, ${TMP} seconds off"
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="Node date/time error"
               fi
            else
               echo "[W] Time on the node off by ${TMP} seconds"
               if [ ${SWNRC} -eq ${SWN_OK} ]; then
                  SWNRC=${SWN_WARNING}
                  SUMMRY="Node date/time issue"
               fi
            fi
         else
            echo "[W] Failed to get from ntppool.org"
            if [ ${SWNRC} -eq ${SWN_OK} ]; then
               SWNRC=${SWN_WARNING}
               SUMMRY="Node date/time issue"
            fi
         fi
      fi
   fi
fi
echo ""
# ########################################################################### #



# check for python3:
# ==================
MYCMD=`/usr/bin/which python3 2>/dev/null`
if [ $? -ne 0 ]; then
   echo "[E] No python3 available"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
      SWNRC=${SWN_CRITICAL}
      SUMMRY="Basic python3 unavailable"
   fi
else
   # get python version for log file:
   ${MYCMD} -V
   #
   TMP=`${MYCMD} -c "import os,sys,platform,time,socket,argparse,traceback,subprocess,re,xml.etree.ElementTree,json,urllib.request,urllib.error,difflib,dateutil.parser,signal;sys.exit(0)" 2>&1`
   if [ $? -ne 0 ]; then
      echo "[E] Missing python3 module\(s\):"
      echo -e "${TMP}" | /usr/bin/awk -F: '/ModuleNotFoundError:/ {printf "   %s\n",$2}'
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="Basic python3 issue"
      fi
   fi
   unset TMP
fi
unset MYCMD
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   if [ -n "${SAM_MODE}" ]; then
      SAMRC=`swn2sam ${SWNRC}`
      echo -e "\n${SUMMRY}\n"
      exit ${SAMRC}
   elif [ -n "${PSST_MODE}" ]; then
      PSSTRC=`swn2psst ${SWNRC}`
      exit ${PSSTRC}
   else
      exit ${SWNRC}
   fi
else
   echo "Python3 with required modules available"
fi
# ########################################################################### #



if [ -n "${SAM_MODE}" ]; then
   SAMRC=`swn2sam ${SWNRC}`
   echo -e "\n${SUMMRY}\n"
   exit ${SAMRC}
elif [ -n "${PSST_MODE}" ]; then
   PSSTRC=`swn2psst ${SWNRC}`
   exit ${PSSTRC}
else
   exit ${SWNRC}
fi
