#!/usr/bin/python3
# ########################################################################### #
#
# SAM WorkerNode SITECONF probe of CMS
#
# ########################################################################### #



import os, sys
import time
import socket
import argparse
import traceback
import subprocess
import re
import xml.etree.ElementTree
import json
import urllib.request, urllib.error
import difflib
import dateutil.parser
# ########################################################################### #



# global variables:
SWNSC_VERSION = "v1.00.00"
#
SWNSC_VERBOSITY = 0
#
SWNSC_SITECONF_PATH = None
SWNSC_SITECONF_FStype = "Unknown"
SWNSC_SITECONF_CVMFS = None
SWNSC_CMSSite = "Unknown"
SWNSC_CMSSubsite = "Unknown"
SWNSC_SiteLocalConfigXML = None
SWNSC_StorageXML = None
SWNSC_StorageJSON = None
SWNSC_STRATUM0_Revision = None
# ########################################################################### #



# CMS SAM Worker Node definitions:
class swnsc:
    unknown = -1
    ok = 0
    warning = 1
    error = 2
    critical = 3

    @staticmethod
    def code2name(code):
        names = ["ok", "warning", "error", "critical"]
        if (( code >= 0 ) and ( code < len(names) )):
            return names[code]
        else:
            return "unknown"

    @staticmethod
    def code2sam(code):
        samRC = [ 0, 1, 2, 2 ]
        if (( code >= 0 ) and ( code < len(samRC) )):
            return samRC[code]
        else:
            return 3

    @staticmethod
    def code2psst(code):
        psstRC = [ 0, 0, 0, 1 ]
        if (( code >= 0 ) and ( code < len(psstRC) )):
            return psstRC[code]
        else:
            return 0


    @staticmethod
    def log(level, indent, message):
        levelNames = { 'C': "Critical", 'E': "Error", 'W': "Warning",
                       'N': "Notice",   'I': "Info",  'D': "Debug",
                       'X': "X-Debug",  '?': "Unknown" }
        #
        msg = None
        try:
            level = levelNames[level][0]
        except:
            level = '?'
        try:
            indent = min( 8, max(0, int(indent)))
        except:
            indent = 0
        newline = 0
        while( message[0] == "\n" ):
            message = message[1:]
            newline += 1
        trailing = 0
        while( message[-1] == "\n" ):
            message = message[:-1]
            trailing += 1
        #
        if ( SWNSC_VERBOSITY >= 2 ):
            if ( level not in "CEWNIDX" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += ( "   " * indent ) \
                           + time.strftime("%H:%M:%S",
                                           time.gmtime(time.time())) \
                           + ( " [%s] " % level ) + line
                else:
                    msg += "\n" + ( "   " * indent ) \
                           + "        " \
                           + ( " [%s] " % level.lower() ) + line
                firstLine = False
        elif ( SWNSC_VERBOSITY == 1 ):
            if ( level not in "CEWNI" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += ( "   " * indent ) + line
                else:
                    msg += "\n" + ( "   " * indent ) + line
                firstLine = False
        elif ( SWNSC_VERBOSITY == 0 ):
            if ( level not in "CEW" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += ( "[%s] " % level ) + line
                else:
                    msg += "\n" + ( "[%s] " % level.lower() ) + line
                firstLine = False
        else:
            if ( level not in "CE" ):
                return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += time.strftime("%Y-%b-%d %H:%M:%S",
                                         time.gmtime(time.time())) \
                           + ( " %s: " % levelNames[level] ) + line
                else:
                    msg += "\n                       " \
                           + ( " " * len(levelNames[level]) ) + line
                firstLine = False
        msg += "\n" * trailing
        #
        print( msg )
# ########################################################################### #



class SamWorkerNodeSideConfAbortException(Exception):
    """Exception to break out of sequential program execution"""
    pass
# ########################################################################### #



def swnsc_timeout_handler(signum, frame):
    raise TimeoutError("Signal %d caught" % signum)
    return
# ########################################################################### #



def swnsc_location():
    """find location of SITECONF area on the node"""
    global SWNSC_SITECONF_PATH
    global SWNSC_SITECONF_FStype
    global SWNSC_SITECONF_CVMFS
    #
    returnCode = swnsc.ok


    try:
        SWNSC_SITECONF_PATH = os.environ['SITECONFIG_PATH']
        swnsc.log("I", 1, "environment defines SITECONFIG_PATH=%s" %
                                                           SWNSC_SITECONF_PATH)
    except KeyError:
        # search for CMS setup:
        setupFile = os.environ.get("VO_CMS_SW_DIR", "") + "/cmsset_default.sh"
        if ( os.path.isfile( setupFile ) ):
            swnsc.log("I", 1, "found CMS setup file at LCG location")
        else:
            setupFile = os.environ.get("OSG_APP", "") + \
                                               "/cmssoft/cms/cmsset_default.sh"
            if ( os.path.isfile( setupFile ) ):
                swnsc.log("I", 1, "found CMS setup file at OSG location")
            else:
                setupFile = os.environ.get("CVMFS", "") + \
                                               "/cms.cern.ch/cmsset_default.sh"
                if ( os.path.isfile( setupFile ) ):
                    swnsc.log("I", 1, "found CMS setup file at CVMFS location")
                else:
                    setupFile = "/cvmfs/cms.cern.ch/cmsset_default.sh"
                    if ( os.path.isfile( setupFile ) ):
                        swnsc.log("I", 1, "using CMS setup file from defau" + \
                                                           "lt CVMFS location")
                    elif ( "OSG_GRID" in os.environ ):
                        setupFile = None
                        initFile = os.environ.get("OSG_GRID", "") + "/setup.sh"
                        if ( os.path.isfile( initFile ) ):
                            myCmd = "/bin/sh -c \". %s && printenv\"" % \
                                                                       initFile
                            for myLine in \
                                       subprocess.getoutput(myCmd).split("\n"):
                                myEnv, myVal = ( myLine.split("=") + [""] )[:2]
                                if ( myEnv == "OSG_APP" ):
                                    setupFile = myVal + \
                                               "/cmssoft/cms/cmsset_default.sh"
                            if (( setupFile is not None ) and
                                ( os.path.isfile( setupFile ) )):
                                swnsc.log("C", 1, "CMS setup file only aft" + \
                                         "er obsolete OSG_GRID initialization")
                            else:
                                swnsc.log("C", 1, "no CMS setup file even " + \
                                      "after obsolete OSG_GRID initialization")
                        else:
                            swnsc.log("C", 1, "no CMS and OSG setup files " + \
                                                                       "found")
                        delete initFile
                        return swnsc.critical
                    else:
                        swnsc.log("C", 1, "no CMS setup file found")
                        return swnsc.critical
        #
        # source setup script and get SITECONFIG_PATH value
        myCmd = "/bin/sh -c \". %s && printenv\"" % setupFile
        for myLine in subprocess.getoutput(myCmd).split("\n"):
            myEnv, myVal = ( myLine.split("=") + [""] )[:2]
            if ( myEnv == "SITECONFIG_PATH" ):
                SWNSC_SITECONF_PATH = myVal
                swnsc.log("N", 1, "CMS setup defines SITECONFIG_PATH=%s" %
                                                           SWNSC_SITECONF_PATH)
                break
        if ( SWNSC_SITECONF_PATH is None ):
            swnsc.log("C", 1, "No SITECONFIG_PATH set by cmsset_default.sh")
            return swnsc.critical

    # resolve symbolic links in SITECONFIG_PATH:
    SWNSC_SITECONF_PATH = os.path.realpath(SWNSC_SITECONF_PATH)

    if ( SWNSC_SITECONF_PATH[-1] != "/" ):
        SWNSC_SITECONF_PATH += "/"
    swnsc.log("N", 1, "using SITECONFIG_PATH \"%s\"" % SWNSC_SITECONF_PATH)


    # check SITECONF area is within a Singularity/Apptainer supported area:
    localSiteconf = os.path.realpath(SWNSC_SITECONF_PATH)
    if (( localSiteconf[:7] != "/cvmfs/" ) and
        ( localSiteconf[:20] != "/etc/cvmfs/SITECONF/" ) and
        ( localSiteconf[:5] != "/cms/" )):
        swnsc.log("W", 1, "SITECONF area seems to be not on a Singularity/Ap" \
                          "ptainer supported mount point!")
        returnCode = swnsc.warning


    # get filesystem type of local SITECONF area:
    mtabRegex = re.compile(r"^([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+")
    try:
        with open("/etc/mtab", 'r') as myFile:
            myLines = myFile.readlines()
        #
        matchLen = 0
        for myLine in myLines:
            matchObj = mtabRegex.match( myLine )
            if ( matchObj is not None ):
                myLen = len( matchObj[2] )
                if ( localSiteconf[:myLen] == matchObj[2] ):
                    if ( myLen > matchLen ):
                        if ( matchObj[3] == "fuse" ):
                            SWNSC_SITECONF_FStype = matchObj[1]
                        else:
                            SWNSC_SITECONF_FStype = matchObj[3]
                        matchLen = myLen
        swnsc.log("I", 1, "Filesystem type of local SITECONF area is \"%s\"" %
                                                         SWNSC_SITECONF_FStype)
    except Exception as excptn:
        swnsc.log("E", 1, ("Failed to identify filesystem type of local SITE" \
                                                "CONF area, %s") % str(excptn))
        returnCode = swnsc.warning
    #
    # get filesystem revision in case of CVMFS:
    if ( SWNSC_SITECONF_FStype == "cvmfs2" ):
        try:
            SWNSC_SITECONF_CVMFS = ( str( os.getxattr(localSiteconf,
                                          "user.fqrn").decode("utf-8") ),
                                     int( os.getxattr(localSiteconf,
                                          "user.revision").decode("utf-8") ) )
            swnsc.log("I", 1, "SITECONF area on CVMFS %s at revision %d" %
                            (SWNSC_SITECONF_CVMFS[0], SWNSC_SITECONF_CVMFS[1]))
        except Exception as excptn:
            swnsc.log("E", 1, ("Failed to get name and revision number of lo" \
                                  "cal SITECONF filesystem, %s") % str(excptn))
            returnCode = swnsc.warning


    return returnCode
# ########################################################################### #



def swnsc_wpad_proxy(wpadURL):
    """fetch wpad proxy auto-config file and find an appropriate web proxy"""

    # get wpad proxy auto-config file:
    try:
        requestObj = urllib.request.Request(wpadURL)
        with urllib.request.urlopen( requestObj, timeout=60 ) as responseObj:
            urlCharset = responseObj.headers.get_content_charset()
            if urlCharset is None:
                urlCharset = "utf-8"
            myData = responseObj.read().decode( urlCharset )
    except urllib.error.URLError as excptn:
        swnsc.log("E", 2, ("Failed to fetch wpad proxy auto-config file %s, " \
                                                "%s") % (wpadURL, str(excptn)))
        return None
    #
    # scan wpad file and take last PROXY entry (catch-all/generic squid):
    proxyRegex = re.compile(r"[\s\"]PROXY ([^;\"\s]+)[;\"\s]+")
    try:
        myProxy = proxyRegex.findall( myData )[-1]
    except IndexError:
        myProxy = None
    del myData
    #
    return myProxy



def swnsc_structure():
    """check directory structure, XML/JSON formats, and mandatory keywords"""
    global SWNSC_CMSSite
    global SWNSC_CMSSubsite
    global SWNSC_SiteLocalConfigXML
    global SWNSC_StorageXML
    global SWNSC_StorageJSON
    #
    returnCode = swnsc.ok

    # check JobConfig area exists:
    if ( os.path.isdir(SWNSC_SITECONF_PATH + "JobConfig") == False ):
        swnsc.log("C", 1, "No JobConfig area in SITECONF")
        return swnsc.critical

    # check site-local-config.xml exists:
    if ( os.path.isfile(SWNSC_SITECONF_PATH +
                        "JobConfig/site-local-config.xml") == False ):
        swnsc.log("C", 1, "No site-local-config.xml file in SITECONF")
        return swnsc.critical

    # try to read site-local-config.xml file:
    try:
        with open(SWNSC_SITECONF_PATH 
                  + "JobConfig/site-local-config.xml", 'r') as myFile:
            SWNSC_SiteLocalConfigXML = myFile.read()
    except Exception as excptn:
        swnsc.log("C", 1, "Failed to read site-local-config.xml, %s" %
                                                                   str(excptn))
        return swnsc.critical

    # try to interprete site-local-config.xml contents as XML:
    try:
        slcfg = xml.etree.ElementTree.fromstring(SWNSC_SiteLocalConfigXML)
    except Exception as excptn:
        swnsc.log("C", 1, ("Failed to interprete site-local-config.xml as XM" \
                                                        "L, %s") % str(excptn))
        return swnsc.critical
    noSites = len( slcfg.findall('site') )
    if ( noSites < 1 ):
        swnsc.log("C", 1, "Zero \"site\" tags in site-local-config.xml")
        return swnsc.critical
    if ( noSites > 1 ):
        swnsc.log("E", 1, "Multiple \"site\" tags in site-local-config.xml")
        returnCode = swnsc.error
    siteElemnt = slcfg.find("site")
    #
    # get CMSSite name:
    try:
        SWNSC_CMSSite = siteElemnt.attrib['name']
    except:
        swnsc.log("E", 1, "Missing \"name\" attribute in \"site\" tag")
        returnCode = swnsc.error
    #
    # validate site name:
    siteRegex = re.compile(r"^T\d_[A-Z]{2,2}_\w+$")
    if ( siteRegex.match( SWNSC_CMSSite ) is None ):
        swnsc.log("E", 1, "Invalid CMS site name in site-local-config.xml")
        returnCode = swnsc.error
    #
    #
    # get CMSSubsite name, if any:
    try:
        SWNSC_CMSSubsite = siteElemnt.find("subsite").attrib['name']
        swnsc.log("N", 1, "Subsite \"%s\" of CMS Site \"%s\"" %
                                             (SWNSC_CMSSubsite, SWNSC_CMSSite))
    except:
        SWNSC_CMSSubsite = None
        swnsc.log("N", 1, "Main site of CMS Site \"%s\"" % SWNSC_CMSSite)
    #
    # validate sub-site name:
    subsiteRegex = re.compile(r"^[A-Za-z0-9][-_A-Za-z0-9]*$")
    if (( SWNSC_CMSSubsite is not None ) and
        ( subsiteRegex.match( SWNSC_CMSSubsite ) is None )):
        swnsc.log("E", 1, "Invalid CMS sub-site name in site-local-config.xml")
        returnCode = swnsc.error

    # check for mandatory tags:
    for mndTag in [ "calib-data" ]:
        myTag = siteElemnt.find( mndTag )
        if ( myTag is None ):
            swnsc.log("E", 1, ("Missing mandatory tag <%s> in JobConfig/site" \
                                                 "-local-config.xml") % mndTag)
            returnCode = swnsc.error
        elif ( mndTag == "calib-data" ):
            #
            # check FroNtier config:
            myTag = myTag.find( "frontier-connect" )
            if ( myTag is None ):
                swnsc.log("E", 1, "Missing mandatory tag <frontier-connect> " \
                                          "in JobConfig/site-local-config.xml")
                returnCode = swnsc.error
            else:
                #
                # check web proxy:
                try:
                    proxyURL = siteElemnt.find("calib-data")\
                                         .find("frontier-connect")\
                                         .find("proxy").attrib['url'].lower()
                    swnsc.log("D", 1, "Web proxy server available, \"%s\"" %
                                                                      proxyURL)
                    del proxyURL
                except:
                    try:
                        proxyURL = swnsc_wpad_proxy( siteElemnt\
                                                     .find("calib-data")\
                                                     .find("frontier-connect")\
                                                     .find("proxyconfig")\
                                                     .attrib['url'].lower() )
                        swnsc.log("D", 1, "Web proxy server available, \"%s\""
                                                                    % proxyURL)
                        del proxyURL
                    except:
                        swnsc.log("W", 1, "No web proxy server found in JobC" \
                                                 "onfig/site-local-config.xml")


    # find all PhEDEx/storage.xml based access:
    # -----------------------------------------
    tfcRegex = re.compile(
                       r".*trivialcatalog_file:([^:?]+)\?.*protocol=([^&]+).*")
    storageXML = set()
    try:
        myTags = siteElemnt.find("event-data").findall("catalog")
        if ( len(myTags) > 0 ):
            for myTag in myTags:
                matchObj = tfcRegex.match( myTag.attrib['url'] )
                if ( matchObj is None ):
                    swnsc.log("W", 1, "Bad <catalog> tag in <event-data> sec" \
                                               "tion of site-local-config.xml")
                    if ( returnCode == swnsc.ok ):
                        returnCode = swnsc.warning
                    continue
                storageXML.add( (matchObj[1], matchObj[2]) )
        else:
            swnsc.log("E", 1, "No <catalog> tag in <event-data> section of J" \
                                              "obConfig/site-local-config.xml")
            returnCode = swnsc.error
    except AttributeError:
        swnsc.log("E", 1, "No <event-data> tag found in JobConfig/site-local" \
                                                                 "-config.xml")
        returnCode = swnsc.error
    except Exception as excptn:
        swnsc.log("E", 1, "Error finding PhEDEx/storage.xml based access, %s" %
                                                                   str(excptn))
        returnCode = swnsc.error


    # look for PhEDEX/storage.xml in local-stage-out and fallback-stage-out:
    # ----------------------------------------------------------------------
    try:
        myTag = siteElemnt.find("local-stage-out").find("catalog")
        if ( myTag is not None ):
            try:
                matchObj = tfcRegex.match( myTag.attrib['url'] )
                storageXML.add( (matchObj[1], matchObj[2]) )
            except:
                swnsc.log("W", 1, "Bad <catalog> tag in <local-stage-out> se" \
                                              "ction of site-local-config.xml")
                if ( returnCode == swnsc.ok ):
                    returnCode = swnsc.warning
        else:
            myTag = siteElemnt.find("local-stage-out").find("lfn-prefix")
            if ( myTag is None ):
                swnsc.log("E", 1, "No <catalog> and no <lfn-prefix> tag in <" \
                           "local-stage-out> section of site-local-config.xml")
                returnCode = swnsc.error
    except AttributeError:
        swnsc.log("E", 1, "No <local-stage-out> tag found in JobConfig/site-" \
                                                            "local-config.xml")
        returnCode = swnsc.error
    except Exception as excptn:
        swnsc.log("E", 1, ("Error finding PhEDEx/storage.xml based stage-out" \
                                                         ", %s") % str(excptn))
        returnCode = swnsc.error
    #
    try:
        myTag = siteElemnt.find("fallback-stage-out").find("catalog")
        if ( myTag is not None ):
            try:
                matchObj = tfcRegex.match( myTag.attrib['url'] )
                storageXML.add( (matchObj[1], matchObj[2]) )
            except:
                swnsc.log("W", 1, "Bad <catalog> tag in <fallback-stage-out>" \
                                           " section of site-local-config.xml")
                if ( returnCode == swnsc.ok ):
                    returnCode = swnsc.warning
        else:
            myTag = siteElemnt.find("fallback-stage-out").find("lfn-prefix")
            if ( myTag is None ):
                swnsc.log("W", 1, "No <catalog> and no <lfn-prefix> tag in <" \
                        "fallback-stage-out> section of site-local-config.xml")
                if ( returnCode == swnsc.ok ):
                    returnCode = swnsc.warning
    except AttributeError:
        swnsc.log("I", 1, "No <fallback-stage-out> tag found in JobConfig/si" \
                                                         "te-local-config.xml")
    except Exception as excptn:
        swnsc.log("E", 1, ("Error finding PhEDEx/storage.xml based stage-out" \
                                                         ", %s") % str(excptn))


    # find storage.json based data access:
    # ------------------------------------
    storageJSON = set()
    try:
        myTags = siteElemnt.find("data-access").findall("catalog")
        if ( len(myTags) > 0 ):
            for myTag in myTags:
                try:
                    mySite = myTag.attrib['site']
                except KeyError:
                    mySite = SWNSC_CMSSite
                try:
                    myVolm = myTag.attrib['volume']
                    myProt = myTag.attrib['protocol']
                except KeyError:
                    swnsc.log("W", 1, "Bad <catalog> tag in <data-access> se" \
                                     "ction ofJobConfig/site-local-config.xml")
                    if ( returnCode == swnsc.ok ):
                        returnCode = swnsc.warning
                    continue
                storageJSON.add( (mySite, myVolm, myProt, "ro") )
                swnsc.log("D", 1, "storage.json reference %s|%s|%s|ro" %
                                                      (mySite, myVolm, myProt))
        else:
            swnsc.log("E", 1, "No <catalog> tag in <data-access> section of " \
                                             "JobConfig/site-local-config.xml")
            returnCode = swnsc.error
    except AttributeError:
        swnsc.log("E", 1, "No <data-access> tag found in JobConfig/site-loca" \
                                                                "l-config.xml")
        returnCode = swnsc.error
    except Exception as excptn:
        swnsc.log("E", 1, ("Error finding storage.json based data access in " \
                          "JobConfig/site-local-config.xml, %s") % str(excptn))
        returnCode = swnsc.error


    # find storage.json references in stage-out:
    # ------------------------------------------
    try:
        myTags = siteElemnt.find("stage-out").findall("method")
        if ( len(myTags) > 0 ):
            for myTag in myTags:
                try:
                    mySite = myTag.attrib['site']
                except KeyError:
                    mySite = SWNSC_CMSSite
                try:
                    myVolm = myTag.attrib['volume']
                    myProt = myTag.attrib['protocol']
                except KeyError:
                    swnsc.log("W", 1, "Bad <method> tag in <stage-out> secti" \
                                       "on of JobConfig/site-local-config.xml")
                    if ( returnCode == swnsc.ok ):
                        returnCode = swnsc.warning
                    continue
                storageJSON.add( (mySite, myVolm, myProt, "rw") )
                swnsc.log("D", 1, "storage.json reference %s|%s|%s|rw" %
                                                      (mySite, myVolm, myProt))
        else:
            swnsc.log("E", 1, "No <method> tag in <stage-out> section of Job" \
                                                "Config/site-local-config.xml")
            returnCode = swnsc.error
    except AttributeError:
        swnsc.log("E", 1, "No <stage-out> tag found in JobConfig/site-local-" \
                                                                  "config.xml")
        returnCode = swnsc.error
    except Exception as excptn:
        swnsc.log("E", 1, ("Error finding storage.json based stage-out in Jo" \
                            "bConfig/site-local-config.xml, %s") % str(excptn))
        returnCode = swnsc.error


    # check TFCs are accessible and contain an LFN-to-PFN for the protocol:
    # ---------------------------------------------------------------------
    strgxmlRegex = re.compile(
                           r".*/(T\d_[A-Z]{2,2}_\w+|local)/PhEDEx/storage.xml")
    for myTpl in sorted(storageXML):
        try:
            with open(myTpl[0], 'r') as myFile:
                myData = myFile.read()
        except Exception as excptn:
            swnsc.log("E", 1, "Failed to access referenced %s, %s" % (myTpl[0],
                                                                  str(excptn)))
            returnCode = swnsc.error
            continue
        #
        matchObj = strgxmlRegex.match( myTpl[0] )
        if ( matchObj is not None ):
            if (( matchObj[1] == SWNSC_CMSSite ) or
                ( matchObj[1] == "local" )):
                SWNSC_StorageXML = myData
        #
        try:
            myTFC = xml.etree.ElementTree.fromstring(myData)
        except Exception as excptn:
            swnsc.log("E", 1, "Failed to interprete %s as XML, %s" %
                                                       (myTpl[0], str(excptn)))
            returnCode = swnsc.error
            continue
        del myData
        #
        for myTag in myTFC.findall('lfn-to-pfn'):
            if ( myTag.attrib['protocol'] == myTpl[1] ):
                swnsc.log("I", 1, ("Found an \"lfn-to-pfn\" tag for protocol" \
                                       " \"%s\" in %s") % (myTpl[1], myTpl[0]))
                break
        else:
            swnsc.log("E", 1, ("No \"lfn-to-pfn\" tag for referenced protoco" \
                                      "l \"%s\" in %s") % (myTpl[1], myTpl[0]))
            returnCode = swnsc.error

    #
    # check storage.json are accessible and contain volume/protocol entry:
    # --------------------------------------------------------------------
    prevFilepath = ""
    myJSON = None
    for myTpl in sorted(storageJSON):
        if ( myTpl[0] == SWNSC_CMSSite ):
            if ( SWNSC_CMSSubsite is None ):
                myFilepath = SWNSC_SITECONF_PATH + "storage.json"
            else:
                myFilepath = SWNSC_SITECONF_PATH + "../storage.json"
        else:
            if ( SWNSC_CMSSubsite is None ):
                myFilepath = SWNSC_SITECONF_PATH + "../" + \
                                                     myTpl[0] + "/storage.json"
            else:
                myFilepath = SWNSC_SITECONF_PATH + "../../" + \
                                                     myTpl[0] + "/storage.json"
        if ( myFilepath != prevFilepath ):
            try:
                with open(myFilepath, 'r') as myFile:
                    myData = myFile.read()
            except Exception as excptn:
                swnsc.log("E", 1, ("Failed to access referenced storage.json" \
                     " of %s at %s, %s") % (myTpl[0], myFilepath, str(excptn)))
                returnCode = swnsc.error
                continue
            #
            if ( myTpl[0] == SWNSC_CMSSite ):
                SWNSC_StorageJSON = myData
            #
            try:
                myJSON = json.loads(myData)
            except Exception as excptn:
                swnsc.log("E", 1, "Failed to interprete %s as JSON, %s" %
                                                     (myFilepath, str(excptn)))
                returnCode = swnsc.error
                continue
            del myData
            #
            prevFilepath = myFilepath
        #
        try:
            for myVolm in myJSON:
                if (( myVolm['site'] != myTpl[0] ) or
                    ( myVolm['volume'] != myTpl[1] )):
                    continue
                for myProt in myVolm['protocols']:
                    if ( myProt['protocol'] != myTpl[2] ):
                        continue
                    if ( myProt['access'] == "virtual" ):
                        continue
                    if (( myProt['access'][-2:] == "ro" ) and
                        ( myTpl[3] == "rw" )):
                        continue
                    if (( myProt['access'][:-3] == "site" ) and
                        ( myTpl[0] != SWNSC_CMSSite )):
                        continue
                    swnsc.log("I", 1, ("Protocol entry for %s|%s|%s|%s in %s")
                        % (myTpl[0], myTpl[1], myTpl[2], myTpl[3], myFilepath))
                    break
                else:
                    swnsc.log("E", 1, ("No entry for referenced access %s|%s" \
                                         "|%s|%s in %s") % (myTpl[0], myTpl[1],
                                               myTpl[2], myTpl[3], myFilepath))
                    returnCode = swnsc.error
                break
            else:
                swnsc.log("E", 1, ("No volume entry for referenced access %s" \
                               "|%s in %s") % (myTpl[0], myTpl[1], myFilepath))
                returnCode = swnsc.error
        except Exception as excptn:
            swnsc.log("E", 1, ("Missing keywords in storage.json of %s at %s" \
                                 ", %s") % (myTpl[0], myFilepath, str(excptn)))
            returnCode = swnsc.error


    return returnCode
# ########################################################################### #



def swnsc_gitlab_fetch_file(filename):
    """fetch a file from SITECONF GitLab and return content as text string"""
    GITLAB_URL = "https://gitlab.cern.ch/api/v4/projects/siteconf%2F"
    GITLAB_LOC = "/repository/files/"
    GITLAB_RAW = "/raw?ref=master"
    GITLAB_HDR = {'PRIVATE-TOKEN': "kuD9s3WrDGNXZ8BvxFV2"}

    gitname = filename.replace("/", "%2F")
    myData = None

    myURL = GITLAB_URL + SWNSC_CMSSite + GITLAB_LOC + gitname + GITLAB_RAW
    #
    try:
        requestObj = urllib.request.Request(myURL, headers=GITLAB_HDR,
                                                       data=None, method="GET")
        responseObj = urllib.request.urlopen(requestObj, timeout=60)
        #
        urlCharset = responseObj.headers.get_content_charset()
        if ( urlCharset is None ):
            urlCharset = "utf-8"
        myData = responseObj.read().decode( urlCharset )
        responseObj.close()
        del urlCharset
        del responseObj
        #
        # sanity check:
        if ( len(myData) < 64 ):
            swnsc.log("E", 1, ("fetched GitLab file, \"%s\" failed sanity ch" \
                              "eck, %d Bytes") % (filename, len(myData)))
            return None
            
    except urllib.error.URLError as excptn:
        swnsc.log("E", 1, "Failed to fetch file \"%s\" from GitLab, %s" %
                                                       (filename, str(excptn)))
        return None

    return myData



def swnsc_gitlab_get_commit(filename):
    """fetch a file info from SITECONF GitLab and return commit time"""
    GITLAB_URL = "https://gitlab.cern.ch/api/v4/projects/siteconf%2F"
    GITLAB_LOC = "/repository/files/"
    GITLAB_MTD = "?ref=master"
    GITLAB_CMT = "/repository/commits/"
    GITLAB_HDR = {'PRIVATE-TOKEN': "kuD9s3WrDGNXZ8BvxFV2"}

    gitname = filename.replace("/", "%2F")
    commitTIS = None

    myURL = GITLAB_URL + SWNSC_CMSSite + GITLAB_LOC + gitname + GITLAB_MTD
    try:
        # fetch file information from GitLab:
        requestObj = urllib.request.Request(myURL, headers=GITLAB_HDR,
                                                       data=None, method="GET")
        with urllib.request.urlopen( requestObj, timeout=60 ) as responseObj:
            urlCharset = responseObj.headers.get_content_charset()
            if urlCharset is None:
                urlCharset = "utf-8"
            myData = responseObj.read().decode( urlCharset )
            del urlCharset
    except urllib.error.URLError as excptn:
        swnsc.log("E", 1, ("Failed to fetch metadata info from GitLab for %s" \
                                             ", %s") % (filename, str(excptn)))
        return None
    #
    # extract last commit id of file:
    try:
        myJSON = json.loads(myData)
        myLastCommit = myJSON['last_commit_id']
        del myJSON
    except json.JSONDecodeError as excptn:
        swnsc.log("E", 1, ("Failed to interprete GitLab metadata info for %s" \
                                                        " as JSON") % filename)
        return None
    except KeyError:
        swnsc.log("E", 1, "No last commit entry in GitLab metadata info of %s"
                                                                    % filename)
        return None
    del myData
    #
    # fetch commit information from GitLab:
    myURL = GITLAB_URL + SWNSC_CMSSite + GITLAB_CMT + myLastCommit
    try:
        requestObj = urllib.request.Request(myURL, headers=GITLAB_HDR,
                                                       data=None, method="GET")
        with urllib.request.urlopen( requestObj, timeout=60 ) as responseObj:
            urlCharset = responseObj.headers.get_content_charset()
            if urlCharset is None:
                urlCharset = "utf-8"
            myData = responseObj.read().decode( urlCharset )
            del urlCharset
    except urllib.error.URLError as excptn:
        swnsc.log("E", 1, "Failed to fetch commit info from GitLab for %s, %s"
                                                     % (filename, str(excptn)))
        return None
    #
    # extract commit time:
    try:
        myJSON = json.loads(myData)
        myAuthoredDate = myJSON['authored_date']
        del myJSON
    except json.JSONDecodeError as excptn:
        swnsc.log("E", 1, ("Failed to interprete GitLab metadata info for %s" \
                                                        " as JSON") % filename)
        return None
    except KeyError:
        swnsc.log("E", 1, "No last commit entry in GitLab metadata info of %s"
                                                                    % filename)
        return None    
    del myData
    #
    try:
        myDatetime = dateutil.parser.parse(myAuthoredDate, yearfirst=True)
        commitTIS = int( myDatetime.timestamp() )
        del myDatetime
    except ValueError as excptn:
        swnsc.log("E", 1, "Failed to interprete GitLab timestring \"%s\", %s" %
                                                 (myAuthoredDate, str(excptn)))
        return None    

    return commitTIS



def swnsc_file_compare(lclData, gitData, lclName, gitName, flag = ""):
    """compare local and GitLab file data, ignore subsite tag if requested"""
    MNFST_URL = "https://cmssst.web.cern.ch/cvmfs/"
    #
    global SWNSC_STRATUM0_Revision

    if (( lclData is None ) or ( gitData is None )):
        return swnsc.unknown

    if ( lclData == gitData ):
        swnsc.log("N", 2, "Local %s and GitLab %s are identical" %
                                                            (lclName, gitName))
        return swnsc.ok

    # line-by-line comparison:
    diffList = list( difflib.unified_diff(lclData.splitlines(keepends=True),
                                          gitData.splitlines(keepends=True),
                           fromfile="local:"+lclName, tofile="GitLab:"+gitName,
                                                                         n=0) )
    if ( flag == "subsite:ignore" ):
        identicalFlag = True
        for myLine in diffList:
            if (( myLine[:4] == "--- " ) or
                (( myLine[:3] == "@@ " ) and ( myLine[-4:] == " @@\n" ))):
                continue
            if ( myLine.find("<subsite ") >= 0 ):
                continue
            identicalFlag = False
        if ( identicalFlag == True ):
            swnsc.log("N", 2, ("Local %s and GitLab %s are identical apart" \
                                     " from subsite tag") % (lclName, gitName))
            return swnsc.ok

    diffStrng = ""
    for myLine in diffList:
        diffStrng += "\n" + myLine.strip("\n\r")
    swnsc.log("I", 2, diffStrng[1:] )
    del diffStrng, diffList

    # get time of local file and GitLab:
    lclModTIS = int( os.stat(SWNSC_SITECONF_PATH + lclName).st_mtime )
    swnsc.log("I", 2, "local %s last modified %s UTC" % (lclName,
                   time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(lclModTIS))))
    #
    gitModTIS = swnsc_gitlab_get_commit(gitName)
    if ( gitModTIS is None ):
        return swnsc.unknown
    swnsc.log("I", 2, "GitLab %s last modified %s UTC" % (gitName,
                   time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(gitModTIS))))

    now = int( time.time() )
    if ( gitModTIS < lclModTIS ):
        # missing GitLab update:
        if ( lclModTIS < (now - (7*24*3600)) ):
            swnsc.log("E", 2, "GitLab of %s out-dated" % gitName)
            return swnsc.error
        elif ( lclModTIS < (now - 3600) ):
            swnsc.log("W", 2, "GitLab of %s not up-to-date" % gitName)
            return swnsc.warning
    else:
        # missing local SITECONF update:
        if (( SWNSC_SITECONF_FStype == "cvmfs" ) and
            ( SWNSC_SITECONF_CVMFS is not None ) and
            ( SWNSC_STRATUM0_Revision is None )):
            # fetch CVMFS manifest for revision number and timestamp:
            mnfstRegex = re.compile(r"^S(\d+)$", flags=re.MULTILINE)
            myData = None
            try:
                myURL = MNFST_URL + SWNSC_SITECONF_CVMFS[0] + "/.cvmfspublished"
                requestObj = urllib.request.Request(myURL)
                with urllib.request.urlopen( requestObj,
                                             timeout=60 ) as responseObj:
                    urlCharset = responseObj.headers.get_content_charset()
                    if urlCharset is None:
                        urlCharset = "utf-8"
                    myBytes = responseObj.read()
                myIndex = myBytes.find(b"\n--\n")
                myData = myBytes[:myIndex - 1].decode( urlCharset )
                del urlCharset
                del myBytes
                #
                matchObj = mnfstRegex.search( myData )
                if ( matchObj is not None ):
                    SWNSC_STRATUM0_Revision = matchObj[1]
                else:
                    swnsc.log("E", 2, "Failed to find revision number in CVM" \
                                                                 "FS manifest")
                    SWNSC_STRATUM0_Revision = 0
            except Exception as excptn:
                swnsc.log("E", 2, "Failed to fetch CVMFS manifest, %s" %
                                                                   str(excptn))
                SWNSC_STRATUM0_Revision = 0
        if (( gitModTIS < (now - (4*3600)) ) and
            ( SWNSC_SITECONF_FStype == "cvmfs" )):
            # CVMFS out-dated (Jenkins runs SITECONF update every hour)
            if (( SWNSC_SITECONF_CVMFS is None ) or
                ( SWNSC_STRATUM0_Revision is None ) or
                ( SWNSC_STRATUM0_Revision == 0 )):
                swnsc.log("W", 2, "CVMFS SITECONF area out-dated")
                return swnsc.warning
            elif ( SWNSC_SITECONF_CVMFS[1] < SWNSC_STRATUM0_Revision ):
                swnsc.log("E", 2, ("Local CVMFS %s stuck / %s out-dated on n" \
                                  "ode!") % (SWNSC_SITECONF_CVMFS[0], lclName))
                return swnsc.error
            elif ( SWNSC_SITECONF_CVMFS[0] == "oasis.opensciencegrid.org" ):
                swnsc.log("E", 2, ("OSG CVMFS or Jenkins GitLab--CVMFS updat" \
                                  "e stuck / %s out-dated on CVMFS") % lclName)
                return swnsc.warning
            else:
                swnsc.log("E", 2, ("Jenkins GitLab--CVMFS update stuck / %s " \
                                               "out-dated on CVMFS") % lclName)
                return swnsc.warning
        elif ( gitModTIS < (now - (7*24*3600)) ):
            swnsc.log("E", 2, "Local %s out-dated" % lclName)
            return swnsc.error
        elif ( gitModTIS < (now - (6*3600)) ):
            swnsc.log("W", 2, "Local %s not up-to-date" % lclName)
            return swnsc.warning

    swnsc.log("N", 2, "Local %s and GitLab %s differ" % (lclName, gitName))
    return swnsc.ok



def swnsc_gitlab_check():
    """check files in GitLab for XML/JSON format, keywords, and local copy"""
    #
    returnCode = swnsc.ok


    # check JobConfig/site-local-config.xml:
    filename = "JobConfig/site-local-config.xml"
    if ( SWNSC_CMSSubsite is None ):
        swnsc.log("I", 1, "site JobConfig/site-local-config.xml check:")
        myData = swnsc_gitlab_fetch_file(filename)
        returnCode = swnsc_file_compare(SWNSC_SiteLocalConfigXML, myData,
                                                            filename, filename)
        if (( myData is not None ) and ( returnCode != swnsc.ok )):
            try:
                myXml = xml.etree.ElementTree.fromstring(myData)
                swnsc.log("I", 1, "GitLab %s has valid XML format" % filename)
                del myXml
            except Exception as excptn:
                swnsc.log("E", 1, "Failed to interprete %s as XML, %s" %
                                                       (filename, str(excptn)))
                returnCode = swnsc.error
    else:
        swnsc.log("I", 1, "sub-site JobConfig/site-local-config.xml check:")
        myData = swnsc_gitlab_fetch_file(SWNSC_CMSSubsite + "/" + filename)
        if ( myData is not None ):
            returnCode = swnsc_file_compare(SWNSC_SiteLocalConfigXML, myData,
                                   filename, SWNSC_CMSSubsite + "/" + filename)
            if (( myData is not None ) and ( returnCode != swnsc.ok )):
                try:
                    myXml = xml.etree.ElementTree.fromstring(myData)
                    swnsc.log("I", 1, "GitLab %s has valid XML format" %
                                             SWNSC_CMSSubsite + "/" + filename)
                    del myXml
                except Exception as excptn:
                    swnsc.log("E", 1, "Failed to interprete %s as XML, %s" %
                              (SWNSC_CMSSubsite + "/" + filename, str(excptn)))
                    returnCode = swnsc.error
        else:
            myData = swnsc_gitlab_fetch_file(filename)
            returnCode = swnsc_file_compare(SWNSC_SiteLocalConfigXML, myData,
                                          filename, filename, "subsite:ignore")
            if (( myData is not None ) and ( returnCode != swnsc.ok )):
                try:
                    myXml = xml.etree.ElementTree.fromstring(myData)
                    swnsc.log("I", 1, "GitLab %s has valid XML format" %
                                                                      filename)
                    del myXml
                except Exception as excptn:
                    swnsc.log("E", 1, "Failed to interprete %s as XML, %s" %
                                                       (filename, str(excptn)))
                    returnCode = swnsc.error
    del myData


    # check PhEDEx/storage.xml:
    if ( SWNSC_StorageXML is not None ):
        swnsc.log("I", 1, "site PhEDEx/storage.xml check:")
        filename = "PhEDEx/storage.xml"
        myData = swnsc_gitlab_fetch_file(filename)
        status = swnsc_file_compare(SWNSC_StorageXML, myData,
                                                            filename, filename)
        if (( status == swnsc.error ) or ( status == swnsc.critical )):
            returnCode = swnsc.error
        elif (( status == swnsc.unknown ) and ( returnCode != swnsc.error )):
            returnCode = swnsc.unknown
        elif (( status != swnsc.ok ) and ( returnCode == swnsc.ok )):
            returnCode = status
        if (( myData is not None ) and ( status != swnsc.ok )):
            try:
                myXml = xml.etree.ElementTree.fromstring(myData)
                swnsc.log("I", 1, "GitLab %s has valid XML format" % filename)
                del myXml
            except Exception as excptn:
                swnsc.log("E", 1, "Failed to interprete %s as XML, %s" %
                                                       (filename, str(excptn)))
                returnCode = swnsc.error
        del myData
    else:
        swnsc.log("N", 1, "Compute-only site, no PhEDEx/storage.xml reference")


    # check storage.json:
    if ( SWNSC_StorageJSON is not None ):
        swnsc.log("I", 1, "site storage.json check:")
        filename = "storage.json"
        myData = swnsc_gitlab_fetch_file(filename)
        status = swnsc_file_compare(SWNSC_StorageJSON, myData,
                                                            filename, filename)
        if (( status == swnsc.error ) or ( status == swnsc.critical )):
            returnCode = swnsc.error
        elif (( status == swnsc.unknown ) and ( returnCode != swnsc.error )):
            returnCode = swnsc.unknown
        elif (( status != swnsc.ok ) and ( returnCode == swnsc.ok )):
            returnCode = status
        if (( myData is not None ) and ( status != swnsc.ok )):
            try:
                myJson = json.loads(myData)
                swnsc.log("I", 1, "GitLab %s has valid JSON format" % filename)
                del myJson
            except Exception as excptn:
                swnsc.log("E", 1, "Failed to interprete %s as JSON, %s" %
                                                       (filename, str(excptn)))
                returnCode = swnsc.error
        del myData
    else:
        swnsc.log("N", 1, "Compute-only site, no storage.json reference")


    return returnCode
# ########################################################################### #



if __name__ == '__main__':
    #
    try:
        import signal
        #
        #
        #
        parserObj = argparse.ArgumentParser(description="CMS SAM WorkerNode " +
                                                              "SITECONF probe")
        parserObj.add_argument("-S", dest="sam", action="store_true",
                                     default=False,
                                     help="ETF configuration, SAM mode")
        parserObj.add_argument("-P", dest="psst", action="store_true",
                                     default=False,
                                     help="fast, minimal-output probing, PSS" +
                                                                      "T mode")
        parserObj.add_argument("-t", dest="timeout", type=int, default=180,
                                     help="maximum time probe is allowed to " +
                                                       "take in seconds [180]")
        parserObj.add_argument("-v", dest="verbose", action="count",
                                     help="increase verbosity")
        argStruct = parserObj.parse_args()
        #
        #
        #
        if ( argStruct.sam ):
            SWNSC_VERBOSITY = 1
        elif ( argStruct.psst ):
            SWNSC_VERBOSITY = -1
        if ( argStruct.verbose is not None ):
            SWNSC_VERBOSITY = argStruct.verbose
        #
        #
        #
        # set timeout:
        start_tis = time.time()
        signal.signal(signal.SIGALRM, swnsc_timeout_handler)
        signal.alarm( argStruct.timeout )
        #
        #
        #
        # initialize to ok:
        summaryMSG = "SITECONF check ok"
        statusCode = swnsc.ok
        swnsc.log("N", 0, "Starting CMS SITECONF check of %s on %s UTC" %
                          (socket.gethostname(),
                   time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(start_tis))))
        swnsc.log("N", 1, "WN-03siteconf version %s" % SWNSC_VERSION)
        swnsc.log("N", 2, "python version %d.%d.%d\n" % sys.version_info[0:3])
        #
        #
        #
        # find SITECONF location:
        swnsc.log("N", 0, "\nSITECONF location check:")
        statusCode = swnsc_location()
        swnsc.log("N", 1, "==> %s" % swnsc.code2name(statusCode) )
        if ( statusCode == swnsc.critical ):
            summaryMSG = "critical SITECONF location issue"
            raise SamWorkerNodeSideConfAbortException("SITECONF location")
        #
        #
        #
        # check SITECONF structure:
        swnsc.log("N", 0, "\nChecking SITECONF area structure:")
        status = swnsc_structure()
        swnsc.log("N", 1, "==> %s" % swnsc.code2name(status) )
        if ( status == swnsc.critical ):
            statusCode = swnsc.critical
            summaryMSG = "critical SITECONF structure issue"
            raise SamWorkerNodeSideConfAbortException("SITECONF structure")
        elif ( status == swnsc.error ):
            statusCode = swnsc.error
            summaryMSG = "SITECONF structure error"
        elif (( status == swnsc.unknown ) and ( statusCode != swnsc.error )):
            statusCode = swnsc.unknown
            summaryMSG = "SITECONF structure issue"
        elif (( status != swnsc.ok ) and ( statusCode == swnsc.ok )):
            statusCode = status
            summaryMSG = "SITECONF structure issue"
        #
        #
        #
        # check GitLab files for syntax and missing keywords:
        if ( not argStruct.psst ):
            swnsc.log("N", 0, "\nComparing with GitLab repository:")
            status = swnsc_gitlab_check()
            swnsc.log("N", 1, "==> %s" % swnsc.code2name(status) )
            if ( status == swnsc.critical ):
                statusCode = swnsc.critical
                summaryMSG = "critical SITECONF GitLab issue"
                raise SamWorkerNodeSideConfAbortException("SITECONF structure")
            elif ( status == swnsc.error ):
                statusCode = swnsc.error
                summaryMSG = "SITECONF GitLab error"
            elif (( status == swnsc.unknown ) and
                  ( statusCode != swnsc.error )):
                statusCode = swnsc.unknown
                summaryMSG = "SITECONF GitLab issue"
            elif (( status != swnsc.ok ) and ( statusCode == swnsc.ok )):
                statusCode = status
                summaryMSG = "SITECONF GitLab issue"

    except SamWorkerNodeSideConfAbortException:
        pass

    except TimeoutError as excptn:
        delta = time.time() - start_tis
        swnsc.log("E", 0, "Maximum execution time exceeded, %.3f sec" % delta)
        statusCode = swnsc.error
        #
        if ( argStruct.sam ):
            print("\nExecution timeout\n")
            sys.exit(swnsc.code2sam(statusCode))
        elif ( argStruct.psst ):
            sys.exit(swnsc.code2psst(statusCode))
        else:
            sys.exit(statusCode)


    except Exception as excptn:
        swnsc.log("E", 0, "Execution failed, %s" % str(excptn))
        statusCode = swnsc.error
        #
        swnsc.log("E", 0, traceback.format_exc() )
        #
        if ( argStruct.sam ):
            print("\nExecution failure\n")
            sys.exit(swnsc.code2sam(statusCode))
        elif ( argStruct.psst ):
            sys.exit(swnsc.code2psst(statusCode))
        else:
            sys.exit(statusCode)

    if ( argStruct.sam ):
        print("\n%s\n" % summaryMSG)
        sys.exit(swnsc.code2sam(statusCode))
    elif ( argStruct.psst ):
        sys.exit(swnsc.code2psst(statusCode))
    else:
        sys.exit(statusCode)
