#!/bin/sh
# ########################################################################### #
#
# SAM WorkerNode CVMFS probe of CMS
#
# ########################################################################### #
# cancel timeout_handler if alive:
trap '(/usr/bin/ps -p ${ALARM_PID}; if [ $? -eq 0 ]; then kill -ABRT ${ALARM_PID}; fi) 1>/dev/null 2>&1' 0
# trap SIGALRM to handle timeouts properly:
trap 'if [ -n "${SAM_MODE}" ]; then echo -e "\nExecution timeout\n"; exit 2; elif [ -n "${PSST_MODE}" ]; then exit 0; else exit 3; fi' 14
#
#
#
SWN_VERSION="v1.00.00"
SWN_CVMFSES="cms.cern.ch oasis.opensciencegrid.org singularity.opensciencegrid.org"
SWN_VERSION_BAD="0. 1. 2.0. 2.1. 2.2. 2.3. 2.4. 2.5. 2.6. 2.7. 2.8."
SWN_VERSION_WARN="2.9.0"
#
#
#
SWN_OK=0
SWN_WARNING=1
SWN_UNKNOWN=2
SWN_ERROR=3
SWN_CRITICAL=4
#
swn2sam () {
   if [ $1 -eq 0 ]; then
      return 0
   elif [ $1 -eq 1 ]; then
      return 1
   elif [ $1 -eq 3 -o $1 -eq 4 ]; then
      return 2
   else
      return 3
   fi
}
#
swn2psst () {
   if [ $1 -eq 4 ]; then
      return 1
   else
      return 0
   fi
}
#
timeout_handler () {
   sleep ${TIMEOUT}
   kill -ALRM $1
}
#
network_check () {
   if [ -x /usr/sbin/ifconfig ]; then
      SWN_NETWRK=`/usr/sbin/ifconfig -a | /usr/bin/awk 'BEGIN{c=""}{n=length($1);if(substr($0,n,1)==":"){d=substr($0,1,n-1)}else{if($1=="inet"){if(substr($2,1,4)!="127."){if((substr($2,1,3)=="10.")||(substr($2,1,7)=="172.16.")||(substr($2,1,7)=="172.17.")||(substr($2,1,7)=="172.18.")||(substr($2,1,7)=="172.19.")||(substr($2,1,7)=="172.20.")||(substr($2,1,7)=="172.21.")||(substr($2,1,7)=="172.22.")||(substr($2,1,7)=="172.23.")||(substr($2,1,7)=="172.24.")||(substr($2,1,7)=="172.25.")||(substr($2,1,7)=="172.26.")||(substr($2,1,7)=="172.27.")||(substr($2,1,7)=="172.28.")||(substr($2,1,7)=="172.29.")||(substr($2,1,7)=="172.30.")||(substr($2,1,7)=="172.31.")||(substr($2,1,8)=="192.168.")){c=c",IPv4-local"}else{c=c",IPv4-global"}}}else{if($1=="inet6"){if((substr($2,1,3)!="::1")&&(substr($2,1,5)!="fe80:")){c=c",IPv6"}}}}}END{print substr(c,2)}'`
   else
      SWN_NETWRK="unknown"
   fi
}
#
SWNRC=${SWN_OK}
SUMMRY="Basic check ok"
# ########################################################################### #



# handle command line options:
SAM_MODE=""
PSST_MODE=""
VERBOSE=0
TIMEOUT=90
IARG=1
while [ ${IARG} -le $# ]; do
   case "${!IARG}" in
   -S) SAM_MODE="yes"
       ;;
   -P) PSST_MODE="yes"
       ;;
   -t) IARG=`expr ${IARG} + 1`
       TIMEOUT=${!IARG}
       ;;
   -v) VERBOSE=`expr ${VERBOSE} + 1`
       ;;
   -vv) VERBOSE=2
       ;;
   -vvv) VERBOSE=3
       ;;
   -vvvv) VERBOSE=4
       ;;
   *) echo "usage: $0 [-h] [-S] [-P] [-t TIMEOUT] [-v]"
      echo -e "\nCMS SAM WorkerNode CVMFS probe\n\noptional arguments:"
      echo "  -h, --help  show this help message and exit"
      echo "  -S          ETF configuration, SAM mode"
      echo "  -P          fast, minimal-output probing, PSST mode"
      echo "  -t TIMEOUT  maximum time probe is allowed to take in seconds [90]"
      echo "  -v          increase verbosity"
      exit 0
   esac
   IARG=`expr ${IARG} + 1`
done
timeout_handler $$ &
ALARM_PID=$!
#
#
TMP1=`/usr/bin/hostname -f`
TMP2=`/usr/bin/date +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
echo "Starting CMS CVMFS check of ${TMP1} on ${TMP2} UTC"
echo -e "   WN-02cvmfs version ${SWN_VERSION}\n"
unset TMP2
unset TMP1
echo ""
# ########################################################################### #



# check filesystem implementation of each CVMFS area of interest:
CVMFS_REAL=""
for CVMFS_AREA in ${SWN_CVMFSES}; do
   TMP_PATH=/cvmfs/${CVMFS_AREA}
   # trigger mount in case of auto-mounted area:
   /bin/ls ${TMP_PATH}/.cvmfsdirtab 1>/dev/null 2>&1
   if [ -d ${TMP_PATH} ]; then
      # get filesystem type:
      TMP_FSTYPE=`/usr/bin/awk -vfs=${TMP_PATH} 'BEGIN{t="";m=-1}{if(NF>=3){l=length($2);if(($2==substr(fs,1,l))&&(l>m)){if($3=="fuse"){t=$1}else{t=$3};m=l}}}END{print t}' /etc/mtab`
      echo "Filesystem type of ${CVMFS_AREA} is ${TMP_FSTYPE}"
      if [ "${TMP_FSTYPE}" = "cvmfs" -o "${TMP_FSTYPE}" = "cvmfs2" ]; then
         if [ -z "${CVMFS_REAL}" ]; then
            CVMFS_REAL="${CVMFS_AREA}"
         else
            CVMFS_REAL="${CVMFS_REAL} ${CVMFS_AREA}"
         fi
      elif [ "${TMP_FSTYPE}" != "nfs" -a "${TMP_FSTYPE}" != "nfs4" ]; then
         echo "   [E] Unsupported CVMFS filesystem implementation"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="unsupported CVMFS filesystem implementation"
         fi
      fi
      TMP_FSTYPE=""
   else
      echo "   [C] CVMFS filesystem ${TMP_PATH} unavailable"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="critical CVMFS filesystem missing"
      fi
   fi
   TMP_PATH=""
done
if [ -z "${CVMFS_REAL}" ]; then
   echo "[N] No CVMFS mounts on the node"
fi
if [ -z "${CVMFS_REAL}" -o ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   if [ -n "${SAM_MODE}" ]; then
      SAMRC=`swn2sam ${SWNRC}`
      echo -e "\n${SUMMRY}\n"
      exit ${SAMRC}
   elif [ -n "${PSST_MODE}" ]; then
      PSSTRC=`swn2psst ${SWNRC}`
      exit ${PSSTRC}
   else
      exit ${SWNRC}
   fi
fi
echo ""
# ########################################################################### #



# CVMFS software version:
if [ -z "${PSST_MODE}" ]; then
   TMP_VRSN=""
   if [ -x /usr/bin/cvmfs2 ]; then
      TMP1=`/usr/bin/cvmfs2 -V`
      echo -e "Installed CVMFS version:\n   ${TMP1}"
      #
      TMP_VRSN=`echo "${TMP1}" | /usr/bin/awk 'BEGIN{v="unknown"}{for(i=1;i<NF;i++){if(tolower($i)=="version"){v=$(i+1);exit}}}END{print v}'`
   fi
   if [ -x /usr/bin/rpm ]; then
      echo "Installed RPMs:"
      /usr/bin/rpm -q -a | /usr/bin/awk '/^cvmfs-/ {printf "   %s\n",$1}'
      #
      if [ -z "${TMP_VRSN}" ]; then
         TMP_VRSN=`/usr/bin/rpm -q -a | /usr/bin/awk -F- '/^cvmfs-[0-9]/ {n=split($3,a,".");if(n>=3){printf "%s-%s\n", $2,a[1]}else{print $2};exit}'`
      fi
   fi
   #
   if [ -n "${TMP_VRSN}" ]; then
      TMP1=`echo "${SWN_VERSION_BAD}" | /usr/bin/awk -vv=${TMP_VRSN} 'BEGIN{e="ok"}{for(i=1;i<=NF;i++){l=length($i);if($i==substr(v,1,l)){e="bad"}}}END{print e}'`
      if [ "${TMP1}" != "ok" ]; then
         echo "   [E] obsolete/unsafe CVMFS version"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="obsolete/unsafe CVMFS version"
         fi
      else
         TMP1=`echo "${SWN_VERSION_WARN}" | /usr/bin/awk -vv=${TMP_VRSN} 'BEGIN{e="ok"}{for(i=1;i<=NF;i++){l=length($i);if($i==substr(v,1,l)){e="warn"}}}END{print e}'`
         if [ "${TMP1}" != "ok" ]; then
            echo "   [W] outmoded/antiquated CVMFS version"
            if [ ${SWNRC} -eq ${SWN_OK} ]; then
               SWNRC=${SWN_WARNING}
               SUMMRY="outmoded/antiquated CVMFS version"
            fi
         fi
      fi
      TMP1=""
   else
      echo "   [W] failed to determine CVMFS version"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="unknown CVMFS version"
      fi
   fi
   #
   if [ -x /usr/bin/attr ]; then
      echo "active CVMFS versions:"
      for CVMFS_AREA in ${CVMFS_REAL}; do
         TMP_PATH=/cvmfs/${CVMFS_AREA}
         TMP1=`(/usr/bin/attr -g version ${TMP_PATH} | /usr/bin/awk 'BEGIN{v=-1}{if(NR==2){v=$1;exit}}END{print v}') 2>/dev/null`
         echo "   ${TMP1} for ${CVMFS_AREA}"
         TMP1=""
         TMP_PATH=""
      done
   else
      echo "   [W] no extended attribute command, /usr/bin/attr, active CVMFS versions unknown"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no extended attribute command"
      fi
   fi
   echo ""
fi
# ########################################################################### #



# Check filesystem revisions:
if [ -z "${PSST_MODE}" ]; then
   echo "CVMFS resisions:"
   # fetch CVMFS revisions summary file:
   TMP1=`/usr/bin/curl -f -s -k -m 30 https://cmssst.web.cern.ch/cvmfs/revisions.txt 2>/dev/null`
   RC=$?
   if [ ${RC} -ne 0 -o -z "${TMP1}" ]; then
      echo "[W] failed to fetch revisions summary file, rc=${RC}"
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="failed to fetch revisions summary file"
      fi
   elif [ -x /usr/bin/attr ]; then
      #
      for CVMFS_AREA in ${CVMFS_REAL}; do
         TMP_PATH=/cvmfs/${CVMFS_AREA}
         #
         # get Stratum-0 revision
         TMP2=`echo "${TMP1}" | /usr/bin/awk -vfs=${CVMFS_AREA} '{if($1==fs){print $2;exit}}'`
         #
         # get local CVMFS revision
         TMP3=`(/usr/bin/attr -g revision ${TMP_PATH} | /usr/bin/awk 'BEGIN{e=-1}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
         #
         echo "   ${CVMFS_AREA}: ${TMP3} (local)   ${TMP2} (Stratum-0)"
         if [ ${TMP3} -lt ${TMP2} ]; then
            echo "   [E] local filesystem out-dated"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                 ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="local filesytem revision out-dated"
            fi
         fi
      done
   else
      echo "   [W] no extended attribute command, /usr/bin/attr, CVMFS revisions unknown"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no extended attribute command"
      fi
   fi
   echo ""
fi
# ########################################################################### #



# check recent I/O errors:
if [ -z "${PSST_MODE}" ]; then
   if [ -x /usr/bin/attr ]; then
      for CVMFS_AREA in ${CVMFS_REAL}; do
         TMP_PATH=/cvmfs/${CVMFS_AREA}
         echo "CVMFS area ${CVMFS_AREA}:"
         #
         # check I/O error(s):
         TMP1=`(/usr/bin/attr -g nioerr ${TMP_PATH} | /usr/bin/awk 'BEGIN{e=0}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
         if [ ${TMP1} -ne 0 ]; then
            echo "   ${TMP1} I/O error(s) since mount"
            #
            TMP1=`(/usr/bin/attr -g timestamp_last_ioerr ${TMP_PATH} | /usr/bin/awk 'BEGIN{e=0}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
            echo -n "   last I/O error at ${TMP1}, "
            /usr/bin/date --date="@${TMP1}" +'%Y-%b-%d %H:%M:%S' 2>/dev/null
            #
            # current time:
            TMP2=`(/usr/bin/date +'%s' | /usr/bin/awk '{print $1-3600; exit}') 2>/dev/null`
            if [ ${TMP1} -ge ${TMP2} ]; then
               echo "   [E] I/O error during last hour"
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                    ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="recent CVMFS I/O error(s)"
               fi
            fi
            TMP2=""
         else
            echo "   without I/O error(s)"
         fi
         TMP1=""
      done
   else
      echo "   [W] no extended attribute command, /usr/bin/attr, CVMFS I/O errors unknown"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no extended attribute command"
      fi
   fi
   echo ""
fi
# ########################################################################### #



# check open file and catalog usage against file descriptor limit:
if [ -x /usr/bin/attr ]; then
   MAXFD=-1
   USEFD=0
   CATNO=0
   # for filedescriptor check we need to count all mounted CVMFSes:
   CVMFS_MOUNT=`/usr/bin/awk '{if($1=="cvmfs2"){print $2}}' /etc/mtab`
   #
   for CVMFS_PATH in ${CVMFS_MOUNT}; do
      echo "mounted CVMFS ${CVMFS_PATH}:"
      #
      # max filedescriptors:
      if [ ${MAXFD} -le 0 ]; then
         TMP1=`(/usr/bin/attr -g maxfd ${CVMFS_PATH} | /usr/bin/awk 'BEGIN{e=-1}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
         if [ $? -ne 0 -o ${TMP1} -lt 0 ]; then
            echo "   [E] failed to query max filedescriptors"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                 ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="filedescriptors query failure"
            fi
         else
            echo "   max filedescriptors: ${TMP1}"
            MAXFD=${TMP1}
         fi
      fi
      TMP=""
      #
      # used filedescriptors:
      TMP1=`(/usr/bin/attr -g usedfd ${CVMFS_PATH} | /usr/bin/awk 'BEGIN{e=-1}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
      if [ $? -ne 0 -o ${TMP1} -lt 0 ]; then
         echo "   [E] failed to query used filedescriptors"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="usage query of mounted CVMFS failed"
         fi
      else
         USEFD=`echo "${USEFD} + ${TMP1}" | /usr/bin/bc`
      fi
      TMP=""
      #
      # number of loaded catalogs in filesystem:
      TMP2=`(/usr/bin/attr -g nclg ${CVMFS_PATH} | /usr/bin/awk 'BEGIN{e=-1}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
      if [ $? -ne 0 -o ${TMP2} -lt 0 ]; then
         echo "   [E] failed to query loaded catalogs"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="usage query of mounted CVMFS failed"
         fi
      else
         CATNO=`echo "${CATNO} + 1 + ${TMP2}" | /usr/bin/bc`
      fi
      echo "   ${TMP1} filedescriptors in use, ${TMP2} catalogs loaded"
      TMP2=""
      TMP1=""
   done
   #
   TMP1=`echo "(${MAXFD} * 0.90)/1" | /usr/bin/bc`
   TMP2=`echo "(${MAXFD} * 0.67)/1" | /usr/bin/bc`
   if [ `expr ${MAXFD} - ${USEFD} - ${CATNO}` -le 4096 ]; then
      echo "[C] not enought filedescriptors left"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="filedescriptors exhausted"
      fi
   elif [ `expr ${USEFD} + ${CATNO}` -ge ${TMP1} ]; then
      echo "[E] filedescriptors use too close to limit"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="filedescriptor usage close to limit"
      fi
   elif [ `expr ${USEFD} + ${CATNO}` -ge ${TMP2} ]; then
      echo "[W] filedescriptor usage quite high"
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="high filedescriptor usage"
      fi
   fi
   if [ `expr ${CATNO} \* 20` -ge ${MAXFD} ]; then
      echo "[W] many catalogs loaded"
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="large number of catalog loaded"
      fi
   fi
   if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
      if [ -n "${SAM_MODE}" ]; then
         SAMRC=`swn2sam ${SWNRC}`
         echo -e "\n${SUMMRY}\n"
         exit ${SAMRC}
      elif [ -n "${PSST_MODE}" ]; then
         PSSTRC=`swn2psst ${SWNRC}`
         exit ${PSSTRC}
      else
         exit ${SWNRC}
      fi
   fi
else
   echo "   [W] no extended attribute command, /usr/bin/attr, CVMFS filedescriptor usage unknown"
   if [ "${SWNRC}" -eq ${SWN_OK} ]; then
      SWNRC=${SWN_WARNING}
      SUMMRY="no extended attribute command"
   fi
fi
echo ""
# ########################################################################### #



# Connectivity check
if [ -z "${PSST_MODE}" ]; then
   echo "Connectivity check:"
   # check CVMFS utility functions exist:
   if [ -f /etc/cvmfs/config.sh ]; then
      for CVMFS_AREA in ${CVMFS_REAL}; do
         echo "   CVMFS area ${CVMFS_AREA}:"
         #
         TMP_FQRN=`(. /etc/cvmfs/config.sh; cvmfs_mkfqrn ${CVMFS_AREA}) 2>/dev/null`
         TMP_ORGN=`(. /etc/cvmfs/config.sh; cvmfs_getorg ${TMP_FQRN}) 2>/dev/null`
         #
         TMP1=`(. /etc/cvmfs/config.sh; cvmfs_readconfig ${CVMFS_AREA}; echo -e "\nCVMFS_SERVER_URL=${CVMFS_SERVER_URL}\nCVMFS_HTTP_PROXY=${CVMFS_HTTP_PROXY}\n") 2>/dev/null`
         TMP_SLIST=`echo "${TMP1}" | /usr/bin/awk -vo=${TMP_ORGN} -vf=${TMP_FQRN} -F= '{if($1=="CVMFS_SERVER_URL"){gsub("[,;|]"," ",$2);gsub("@org@",o,$2);gsub("@fqrn@",f,$2);print $2;exit}}'`
         TMP_PLIST=`echo "${TMP1}" | /usr/bin/awk -F= '{if($1=="CVMFS_HTTP_PROXY"){gsub("[,;|]"," ",$2);n=split($2,a," ");for(i=1;i<=n;i++){if(index(a[i],"auto")<=0){print a[i]}};exit}}'`
         TMP1=""
         #
         TMP_PATH=/cvmfs/${CVMFS_AREA}
         TMP_TOUT=`(/usr/bin/attr -g timeout ${CVMFS_PATH} | /usr/bin/awk 'BEGIN{e=30}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
         TMP_DRKT=`(/usr/bin/attr -g timeout_direct ${CVMFS_PATH} | /usr/bin/awk 'BEGIN{e=45}{if(NR==2){e=int($1);exit}}END{print e}') 2>/dev/null`
         TMP_PATH=""
         #
         TMP_CNT=0
         for SRVR in ${TMP_SLIST}; do
            echo "      Stratum-1 ${SRVR}:"
            #
            for PRXY in ${TMP_PLIST}; do
               echo "         via HTTP-proxy ${PRXY}:"
               #
               if [ "${PRXY}" = "DIRECT" ]; then
                  TMP1="/usr/bin/curl -f -s -m 60 --connect-timeout -H \"Pragma\" ${TMP_DRKT} ${SRVR}/.cvmfspublished"
                  TMP2=`/usr/bin/curl -f -s -m 60 --connect-timeout -H "Pragma" ${TMP_DRKT} ${SRVR}/.cvmfspublished 1>/dev/null`
                  RC=$?
               else
                  TMP1="(http_proxy=${PRXY}; /usr/bin/curl -f -s -m 60 --connect-timeout ${TMP_TOUT} -H \"Pragma\" ${SRVR}/.cvmfspublished)"
                  TMP2=`(http_proxy=${PRXY}; /usr/bin/curl -f -s -m 60 --connect-timeout ${TMP_TOUT} -H "Pragma" ${SRVR}/.cvmfspublished) 1>/dev/null`
                  RC=$?
               fi
               if [ ${RC} -eq 0 ]; then
                  echo "            server reached"
                  TMP_CNT=`echo "${TMP_CNT} + 1" | /usr/bin/bc`
               else
                  echo "            \"${TMP1}\" failed with rc=${RC}"
                  echo "${TMP2}" | /usr/bin/awk '{printf "            %s\n",$0}'
                  echo "            failed with rc=${RC}"
                  echo "            [W] failed to reach server"
                  if [ ${SWNRC} -eq ${SWN_OK} ]; then
                     SWNRC=${SWN_WARNING}
                     SUMMRY="a Stratum-1 unreachable"
                  fi
               fi
            done
         done
         TMP_DRKT=""
         TMP_TOUT=""
         TMP_PLIST=""
         TMP_SLIST=""
         TMP_ORGN=""
         TMP_FQRN=""
         if [ ${TMP_CNT} -eq 0 ]; then
            echo "   [E] all Stratum-1s of ${CVMFS_AREA} unreachable"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                 ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="all Stratum-1 unreachable"
            fi
         fi
         TMP_CNT=""
      done
   else
      echo " [W] no CVMFS utility functions file, /etc/cvmfs/config.sh"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no CVMFS utility functions library"
      fi
   fi
   echo ""
fi
# ########################################################################### #



if [ -n "${SAM_MODE}" ]; then
   SAMRC=`swn2sam ${SWNRC}`
   echo -e "\n${SUMMRY}\n"
   exit ${SAMRC}
elif [ -n "${PSST_MODE}" ]; then
   PSSTRC=`swn2psst ${SWNRC}`
   exit ${PSSTRC}
else
   exit ${SWNRC}
fi
